//
//  AbstractViewController.swift
//  ConferenceSeries
//
//  Created by Mallesh Kurva on 20/04/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import UIKit
import MobileCoreServices
import SVProgressHUD_0_8_1
import WebKit


class AbstractViewController: UIViewController,UIDocumentPickerDelegate,UIDocumentMenuDelegate,sendDetails {
    

    @IBOutlet weak var titleBtn: ButtonBorder!
    @IBOutlet weak var cntryBtn: ButtonBorder!
    @IBOutlet weak var categoryBtn: ButtonBorder!
    @IBOutlet weak var selectBtn: ButtonBorder!
    @IBOutlet weak var email:UITextField!
    @IBOutlet weak var phone:UITextField!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var scrollview:UIScrollView!

    @IBOutlet weak var sampleTemplate: UIButton!
    @IBOutlet weak var filename: UILabel!
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var chooseFileBtn: UIButton!
    var trackarr = [String]()
    var countrycode = String()
    let ipc = UIImagePickerController()
    var fileurl = NSURL()
    var isfromsignin = false
    var docURl:URL? = nil

    
    @IBAction func TitleBtnTapped(_ sender: UIButton) {
          let storyBoard = UIStoryboard(name: "Main", bundle: nil)
          let viewTobeLoad = storyBoard.instantiateViewController(withIdentifier: "Country") as! SelectCountryVC
          viewTobeLoad.delegate2 = self
          viewTobeLoad.iscountry = false
          viewTobeLoad.isfromabstract = true
          viewTobeLoad.fields = titlearr
          viewTobeLoad.type = "title"
          self.navigationController?.pushViewController(viewTobeLoad, animated: true)
    }
    
    
    @IBAction func CountryBtnTapped(_ sender: UIButton) {
         let storyBoard = UIStoryboard(name: "Main", bundle: nil)
         let viewTobeLoad = storyBoard.instantiateViewController(withIdentifier: "Country") as! SelectCountryVC
         viewTobeLoad.delegate2 = self
         viewTobeLoad.iscountry = true
         viewTobeLoad.isfromabstract = true
         viewTobeLoad.type = "country"
         self.navigationController?.pushViewController(viewTobeLoad, animated: true)
    }

    @IBAction func CategoryBtnTapped(_ sender: UIButton) {
         let storyBoard = UIStoryboard(name: "Main", bundle: nil)
         let viewTobeLoad = storyBoard.instantiateViewController(withIdentifier: "Country") as! SelectCountryVC
         viewTobeLoad.delegate2 = self
         viewTobeLoad.iscountry = false
         viewTobeLoad.isfromabstract = true
         viewTobeLoad.type = "category"
        viewTobeLoad.fields = categoryarr
         self.navigationController?.pushViewController(viewTobeLoad, animated: true)
    }

    @IBAction func SelectBtnTapped(_ sender: UIButton) {
        if self.trackarr.count > 0
        {
             let storyBoard = UIStoryboard(name: "Main", bundle: nil)
             let viewTobeLoad = storyBoard.instantiateViewController(withIdentifier: "Country") as! SelectCountryVC
             viewTobeLoad.delegate2 = self
             viewTobeLoad.iscountry = false
             viewTobeLoad.isfromabstract = true
             viewTobeLoad.type = "select"
            viewTobeLoad.fields = trackarr
             self.navigationController?.pushViewController(viewTobeLoad, animated: true)
        }
    }
    
    @IBAction func ChooseFileBtnTapped(_ sender: UIButton) {
        
        
        self.openiCloudDocuments()
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Submit Abstract"
        self.getSelecttype()
        self.view.backgroundColor = UIColor.white
        
        DispatchQueue.main.async {

            self.titleBtn.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: -(self.titleBtn.imageView?.frame.size.width)!+(15), bottom: 0, right: (self.titleBtn.imageView?.frame.size.width)!)
            self.titleBtn.imageEdgeInsets = UIEdgeInsets.init(top: 15, left: self.titleBtn.frame.size.width - 10, bottom: 15, right: 2);
                  
            self.cntryBtn.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: -(self.cntryBtn.imageView?.frame.size.width)!+(15), bottom: 0, right: (self.cntryBtn.imageView?.frame.size.width)!)
        
            self.cntryBtn.imageEdgeInsets = UIEdgeInsets.init(top: 15, left: self.cntryBtn.frame.size.width - 10, bottom: 15, right: 2);
                  
            self.categoryBtn.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: -(self.categoryBtn.imageView?.frame.size.width)!+(15), bottom: 0, right: (self.categoryBtn.imageView?.frame.size.width)!)
            self.categoryBtn.imageEdgeInsets = UIEdgeInsets.init(top: 15, left: self.categoryBtn.frame.size.width - 10, bottom: 15, right: 2);
                  
            self.selectBtn.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: -(self.selectBtn.imageView?.frame.size.width)!+(15), bottom: 0, right: (self.selectBtn.imageView?.frame.size.width)!)
            self.selectBtn.imageEdgeInsets = UIEdgeInsets.init(top: 15, left: self.selectBtn.frame.size.width - 10, bottom: 15, right: 2);
        }
        self.sampleTemplate.layer.cornerRadius = 3.0
        self.sampleTemplate.clipsToBounds = true
        
        // UITextfield Delegate
        self.name.delegate = self
        self.email.delegate = self
        self.phone.delegate = self
        self.address.delegate = self
        // Keyboard
        NotificationCenter.default.addObserver(
                   self,
                   selector: #selector(keyboardWillShow),
                   name: UIResponder.keyboardWillShowNotification,
                   object: nil
               )
               NotificationCenter.default.addObserver(
                          self,
                          selector: #selector(keyboardWillHide),
                          name: UIResponder.keyboardWillHideNotification,
                          object: nil
                      )
        
        
        let singleTap = UITapGestureRecognizer(target: self, action: Selector(("handleTap:")))
         singleTap.cancelsTouchesInView = false
         singleTap.numberOfTapsRequired = 1
         scrollview.addGestureRecognizer(singleTap)
        
        
        
        // Registered fields
     /*
        self.cntryBtn.setTitle(userDefaults.value(forKey: "country") as? String, for: .normal)
        self.email.text = userDefaults.value(forKey: "email") as? String
        self.phone.text = userDefaults.value(forKey: "phone") as? String
        self.cntryBtn.isUserInteractionEnabled = false
        self.email.isUserInteractionEnabled = false
        self.phone.isUserInteractionEnabled = false*/

        // Do any additional setup after loading the view.
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        
    // handling code

        name.resignFirstResponder()
        address.resignFirstResponder()
        email.resignFirstResponder()
        address.resignFirstResponder()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardSize.height
            var contentInset:UIEdgeInsets = self.scrollview.contentInset
              contentInset.bottom = keyboardHeight + 10
              scrollview.contentInset = contentInset
          /*  if self.view.frame.origin.y == 0
            {
              //  self.view.frame.origin.y -= getMoveableDistance(keyboarHeight: keyboardHeight)
                self.view.frame.origin.y -= keyboardHeight
            }*/
        }
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
           scrollview.contentInset = contentInset
        
       /* if self.view.frame.origin.y != 0
            {
                self.view.frame.origin.y = 0
            }*/
        }
       
    //get the distance to move up the main view for the focus textfiled
    func getMoveableDistance(keyboarHeight : CGFloat) ->  CGFloat{
        var y:CGFloat = 0.0
        if let activeTF = getSelectedTextField(){
            var tfMaxY = activeTF.frame.maxY
            var containerView = activeTF.superview!
            while containerView.frame.maxY != self.view.frame.maxY{
                let contViewFrm = containerView.convert(activeTF.frame, to: containerView.superview)
                tfMaxY = tfMaxY + contViewFrm.minY
                containerView = containerView.superview!
            }
            let keyboardMinY = self.view.frame.height - keyboarHeight
            if tfMaxY > keyboardMinY{
                y = (tfMaxY - keyboardMinY) + 10.0
            }
        }
    
        return y
    }
    
    @IBAction func Endediting(_ sender: UITapGestureRecognizer) {
      //  self.view.endEditing(true)
    }
    
    // Dowmload sample abstract template
    @IBAction func downloadtemplate(_ sender: UIButton) {
        self.view.addActivity()
        let obj = NetworkManager(utility: networkUtility!)
        let id = userDefaults.value(forKey: "conf_id") as! String
        let params = [kconf_id:id]
        obj.postRequestApiWith(api: kabstractTemplate, params: params){ (response) in
            if response != nil
            {
                
                let dict = response as! NSDictionary
                let status = iskeyexist(dict: dict, key: kstatus)
                if (status == "true" || status == "1")
                {
                    let result = dict.value(forKey: "result") as! NSArray
                    let innerdict = result.firstObject as! NSDictionary
                    let templateurl = iskeyexist(dict: innerdict, key: "abstract_template")
                    if templateurl != ""
                    {
                        self.downloadsample(url:templateurl)
                    }
                    else
                    {
                        self.showAlertWith(title: "Alert", message: "Sample Template is not available")
                    }
                }
                else
                {
                    self.showAlertWith(title: "Alert", message: "Sample Template is not available")
                }
                DispatchQueue.main.async {
                    self.view.removeActivity()
                }
        
            }
            else
            {
    
                DispatchQueue.main.async {
                    self.view.removeActivity()
                }
            }
        }
    }
    func downloadsample(url:String)
    {
        guard let url = URL(string: url) else { return }
               
        let urlSession = URLSession(configuration: .default, delegate: self, delegateQueue: OperationQueue())
               
        let downloadTask = urlSession.downloadTask(with: url)
        downloadTask.resume()
        loaddocument()
    }
    func getSelecttype()
    {
            self.trackarr.removeAll()
            self.view.addActivity()
            let obj = NetworkManager(utility: networkUtility!)
            let id = userDefaults.value(forKey: "conf_id") as! String
            let params = [kconf_id:id]
            obj.postRequestApiWith(api: ktracks, params: params){ (response) in
            if response != nil
            {
                let dict = response as! NSDictionary
                if dict.count > 0
                {
                    if (dict.value(forKey: "tracks") != nil)
                    {
                        let tracks = dict.value(forKey: "tracks") as! NSArray
                        if tracks.count > 0
                        {
                            let trackslist = tracks.map{
                                (item) -> String in
                                (item as! NSDictionary).value(forKey: "TrackName") as! String
                            }
                            self.trackarr = trackslist
                            print(self.trackarr)
                        }
                    }
                     
                }
               
                DispatchQueue.main.async {
                    self.view.removeActivity()
                }
        
            }
            else
            {
                DispatchQueue.main.async {
                    self.view.removeActivity()
                }
            }
        }
    }
    
    // Delegate Implementation
    func details(titlename: String,type:String,countrycode:String) {
          
        switch type{
        case "title":
              self.titleBtn.setTitle(titlename, for: .normal)
            break
        case "country":
            self.cntryBtn.setTitle(titlename, for: .normal)
            self.countrycode = countrycode
            break
        case "category":
            self.categoryBtn.setTitle(titlename, for: .normal)
            break
        case "select":
            self.selectBtn.setTitle(titlename, for: .normal)
            break
        default:
            self.titleBtn.setTitle("Select Title", for: .normal)
            self.cntryBtn.setTitle("Select Country", for: .normal)
            self.categoryBtn.setTitle("Select Category", for: .normal)
            self.selectBtn.setTitle("Select", for: .normal)
            
            break
        }
        self.navigationController?.popViewController(animated: false)
    }
    
    //To Pick Document
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
                  guard let myURL = urls.first else {
                       return
                  }
                  print("import result : \(myURL)")
        self.fileurl = myURL as NSURL
        self.filename.text = "abstract-template.doc"
        }
    
    
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
            documentPicker.delegate = self
            present(documentPicker, animated: true, completion: nil)
        }
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
                print("view was cancelled")
                dismiss(animated: true, completion: nil)
        }
    
    func openiCloudDocuments(){
        let importMenu = UIDocumentPickerViewController(documentTypes: [String("public.data")], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
        
    }
    
  // Submit abstract
    @IBAction func submit(_ sender:UIButton)
    {
        self.view.endEditing(true)
        if (titleBtn.currentTitle == "Select Title") || (name.text?.isEmpty)! || categoryBtn.currentTitle == "Select Category" || selectBtn.currentTitle == "Select trackname" || (address.text?.isEmpty)! || (phone.text?.isEmpty)! || (email.text?.isEmpty)!
        {
            self.showAlertWith(title: "Alert", message: "All fields are Mandatory")
        }
        else
        {
        if titleBtn.currentTitle == "Select Title"
        {
            self.showAlertWith(title:"Alert" , message: "Please Select Title")
        }
        if name.text == ""
        {
            self.showAlertWith(title:"Alert" , message: "Please enter the name")
        }
        if categoryBtn.currentTitle == "Select Category"
        {
            self.showAlertWith(title:"Alert" , message: "Please Select Category")
        }
        if selectBtn.currentTitle == "Select trackname"
        {
            self.showAlertWith(title:"Alert" , message: "Please Select trackname")
        }
        if address.text == ""
        {
            self.showAlertWith(title:"Alert" , message: "Please Select address")
        }
        if filename.text == "No File Choosen"
        {
            self.showAlertWith(title:"Alert" , message: "Please Select abstract")
        }
       let validemail = (isValidEmailAddress(emailAddressString:(email.text!)))
        let validcontact = (isvalidcontact(value: ("\(self.countrycode)" + phone.text!)))
       if validemail == false
       {
           // Show Alert message
           self.showAlertWith(title: "Alert", message: "Please enter valid EmailID")
       }
       if validcontact == false
       {
           // Show Alert message
           self.showAlertWith(title: "Alert", message: "Please enter valid Contact")
       }
            if ((validcontact) && (validemail) && (self.filename.text != "No File Choosen"))
            {

        let dateformatter = DateFormatter()
        dateformatter.dateFormat = kUTCformat
        let currentdate = dateformatter.string(from:Date())
        let obj = NetworkManager(utility: networkUtility!)
        let id = userDefaults.value(forKey: "conf_id") as! String
        let fname = self.name.text!
        let email = self.email.text!
        let country = self.cntryBtn.currentTitle!
        let phone = self.phone.text!
        
                let appuserid = userDefaults.value(forKey: kappuserId)!
                let params = ["conf_id":id,"title":self.titleBtn.currentTitle!,"name":"\(fname)","country":"\(country)","email":"\(email)","phone":"\(phone)","category":self.categoryBtn.currentTitle!,"track_name":self.selectBtn.currentTitle!,"address":self.address.text!,"date":currentdate,"source":"ios","app_user_id":appuserid] as [String : Any]
                
        self.uploadFile(img:fileurl as URL , params: params)
            }
        }
    }
    func isValidEmailAddress(emailAddressString: String) -> Bool {
           
           var returnValue = true
           let emailRegEx = kEmailFormat
           
           do {
               let regex = try NSRegularExpression(pattern: emailRegEx)
               let nsString = emailAddressString as NSString
               let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
               
               if results.count == 0
               {
                   returnValue = false
               }
               
           } catch let error as NSError {
               print("invalid regex: \(error.localizedDescription)")
               returnValue = false
           }
           
           return  returnValue
       }
       func isvalidcontact(value: String) -> Bool {
           let phoneTest = NSPredicate(format: "SELF MATCHES %@", kContactFormat)
           let result =  phoneTest.evaluate(with: value)
           return result
       }
    func uploadFile(img:URL,params:[String:Any])
       {
           var imageData:NSData
           guard let fileData = try? NSData(contentsOf: img) else {
             return
           }
           imageData = fileData
           SVProgressHUD.show(withStatus: "Uploading...")
           self.view.isUserInteractionEnabled = false
           let obj = NetworkManager(utility: networkUtility!)
           obj.uploadImageRequest(api: kinsertabstract, params: params, image: imageData as Data, completion: {
               (response) in
                   SVProgressHUD.setStatus("Uploading.....")
                   guard let result = response as? NSDictionary
                   
                   else
                   {
                       SVProgressHUD.dismiss()
                       self.view.isUserInteractionEnabled = true
                       return
                   }
                   if result["status"] as? Bool == true
                   {
                       
                       SVProgressHUD.setStatus("Uploaded Successfully")
                    self.setdefaults()
                   }
                   else
                   {
                       self.showAlertWith(title: "Upload Failed", message: "Unable to upload Image")
                   }
                   SVProgressHUD.dismiss()
                   self.view.isUserInteractionEnabled = true
           }, onerror: {
               (error) in
            
               SVProgressHUD.dismiss()
               self.showAlertWith(title: "Upload Failed", message: "Unable to upload Document")
               self.view.isUserInteractionEnabled = true

           })
       }
    func setdefaults()
    {
        
        self.titleBtn.setTitle("Select Title", for: .normal)
        self.name.text = ""
        self.cntryBtn.setTitle("Select Country", for: .normal)
        self.email.text = ""
        self.phone.text = ""
        self.categoryBtn.setTitle("Select Category", for: .normal)
        self.selectBtn.setTitle("Select trackname", for: .normal)
        self.address.text = ""
        self.filename.text = "No File Choosen"
    }

    func showAlertWith(title:String,message:String)
        {
            let alertController = UIAlertController(title: title, message:message , preferredStyle: .alert)
            //to change font of title and message.
            let titleFont = [NSAttributedString.Key.font: UIFont(name: kAppFontBold, size: 14.0)!]
            let messageFont = [NSAttributedString.Key.font: UIFont(name: kAppFont, size: 12.0)!]
            
            let titleAttrString = NSMutableAttributedString(string: title, attributes: titleFont)
            let messageAttrString = NSMutableAttributedString(string: message, attributes: messageFont)
            alertController.setValue(titleAttrString, forKey: "attributedTitle")
            alertController.setValue(messageAttrString, forKey: "attributedMessage")
            
            let alertAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
               
            })
    //        let titlealertaction = NSMutableAttributedString(string: "OK", attributes: titleFont)
    //        alertController.setValue(titlealertaction, forKey: "attributedTitle")
            alertController.addAction(alertAction)
            present(alertController, animated: true, completion: nil)
        }
    @IBAction func back(_ sender: UIBarButtonItem) {
        if isfromsignin
        {
            self.navigationController?.popToRootViewController(animated: true)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
       
    }
}
extension AbstractViewController:UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (phone.text?.count)! >= 14
        {
            let  char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            
            if (isBackSpace == -92) {
                return true
            }
            
            return false
        }
       
        if textField.tag == 20
        {
         let valid = isValidEmailAddress(emailAddressString: textField.text!)
          if valid == true
          {
            email.setBottomBorder(color: UIColor.darkGray)
          }
          else
          {
            email.setBottomBorder(color: UIColor(red: 252/255, green: 78/255, blue: 8/255, alpha: 1.0))
          }
        }
        return true
    }
    func getSelectedTextField() -> UITextField?
    {
         let totalTextFields = getTextFieldsInView(view: self.view)
        
            for textField in totalTextFields{
                if textField.isFirstResponder{
                    return textField
                }
            }
            return nil
    }
    func getTextFieldsInView(view: UIView) -> [UITextField] {
    
        var totalTextFields = [UITextField]()
    
        for subview in view.subviews as [UIView] {
            if let textField = subview as? UITextField {
                totalTextFields += [textField]
            } else {
                totalTextFields += getTextFieldsInView(view: subview)
            }
        }
    
        return totalTextFields
    }
    func loaddocument()
    {
        let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        if docURl != nil
        {
            self.showAlertWith(title: "Success", message: "Sample Template document downloaded successfully")
      /*  let destinationURL = documentDirectory.appendingPathComponent(docURl!.lastPathComponent)
        let fileexists = FileManager().fileExists(atPath: destinationURL.path)
        if fileexists
        {
            DispatchQueue.main.async {
                
                let data = try! Data(contentsOf: destinationURL)
                let loadwebview = WKWebView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
                loadwebview.load(data, mimeType: "application/doc", characterEncodingName: "", baseURL: destinationURL.deletingLastPathComponent())
                let docVC = UIViewController()
                docVC.view.addSubview(loadwebview)
                docVC.view.addActivity()
                
                loadwebview.translatesAutoresizingMaskIntoConstraints = false
                loadwebview.leftAnchor.constraint(equalTo: docVC.view.leftAnchor).isActive = true
                loadwebview.rightAnchor.constraint(equalTo: docVC.view.rightAnchor).isActive = true
                loadwebview.topAnchor.constraint(equalTo: docVC.view.topAnchor).isActive = true
                loadwebview.bottomAnchor.constraint(equalTo: docVC.view.bottomAnchor).isActive = true
                docVC.title = destinationURL.lastPathComponent
                self.navigationController?.pushViewController(docVC, animated: true)
                docVC.view.removeActivity()
            }
        }
        else
        {
           print("file not exists")
        }*/
        }
        else
        {
            self.showAlertWith(title: "Alert", message: "Sample Template Download failed")
        }
        
    }
}
extension AbstractViewController:URLSessionDownloadDelegate
{
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        print("downloadLocation:", location)
            // create destination URL with the original pdf name
            guard let url = downloadTask.originalRequest?.url else { return }
            let documentsPath = FileManager.default.urls(for: .documentDirectory , in: .userDomainMask)[0]
            let destinationURL = documentsPath.appendingPathComponent(url.lastPathComponent)
            // delete original copy
            try? FileManager.default.removeItem(at: destinationURL)
            // copy from temp to Document
            do {
                try FileManager.default.copyItem(at: location, to: destinationURL)
                self.docURl = destinationURL
               
            } catch let error {
                print("Copy Error: \(error.localizedDescription)")
            }
        
        }
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        let progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
        print(progress)
//        DispatchQueue.main.async {
//            self.progressDownloadIndicator.progress = progress
//        }
    }
    
}
