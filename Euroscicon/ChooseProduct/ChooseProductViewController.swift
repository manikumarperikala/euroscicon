//
//  ChooseProductViewController.swift
//  ConferenceSeries
//
//  Created by manikumar on 16/03/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import UIKit

class ChooseProductViewController: UIViewController,sentname {
    
    @IBOutlet weak var gbp:UIButton!
    @IBOutlet weak var euro:UIButton!
    @IBOutlet weak var usd:UIButton!
    
    @IBOutlet weak var Category:ButtonBorder!
    @IBOutlet weak var Product:ButtonBorder!
    @IBOutlet weak var yrf:ButtonBorder!
    @IBOutlet weak var eposter:ButtonBorder!
    @IBOutlet weak var early:UILabel!
    @IBOutlet weak var normal:UILabel!
    @IBOutlet weak var final:UILabel!
    @IBOutlet weak var price1:UILabel!
    @IBOutlet weak var price2:UILabel!
    @IBOutlet weak var price3:UILabel!
    
     @IBOutlet weak var early1:UILabel!
     @IBOutlet weak var normal1:UILabel!
     @IBOutlet weak var final1:UILabel!
     @IBOutlet weak var price11:UILabel!
     @IBOutlet weak var price22:UILabel!
     @IBOutlet weak var price33:UILabel!
    
    @IBOutlet weak var early2:UILabel!
    @IBOutlet weak var normal2:UILabel!
    @IBOutlet weak var final2:UILabel!
    @IBOutlet weak var price111:UILabel!
    @IBOutlet weak var price222:UILabel!
    @IBOutlet weak var price333:UILabel!
    @IBOutlet weak var totalPrice:UILabel!
    @IBOutlet weak var collectionView:UICollectionView!
    
    @IBOutlet weak var proceed: ButtonBorder!
    
    @IBOutlet weak var Product1: UIButton!
    @IBOutlet weak var Product2: UIButton!
    @IBOutlet weak var Product3: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var eposterView: CustomView!
    @IBOutlet weak var yrfView: CustomView!
    @IBOutlet weak var academicView:CustomView!
    
    var registrationDetailsArray = NSArray()
  

    var Categoryfields = [String]()
    var pannetype = "GBP"
    var Productarr = NSArray()
    var academicCat = NSArray()
    var bussinessCat = NSArray()
    var studentCat = NSArray()
    var yrfCat = NSArray()
    var eposerCat = NSArray()
    var addOnCat = NSArray()
    var packagenamearr = [String]()
    
    var index = Int()
    var selectprice = String()
    
    var index1 = Int()
    var selectprice1 = String()
    
    var index2 = Int()
    var selectprice2 = String()
    var totalProdPrice = 0
    var addOntotal = 0
    var confDict = NSDictionary()
    var ChoosenProd = [ChoosenProducts]()
    var prods = [NSMutableDictionary]()
    var addonprods = [NSMutableDictionary]()
    var choosenProdDict = NSDictionary()
    
    @IBOutlet weak var eposterViewHgt: NSLayoutConstraint!
    @IBOutlet weak var addOnViewHgt: NSLayoutConstraint!
    @IBOutlet weak var yrfViewHgt: NSLayoutConstraint!
    @IBOutlet weak var academicViewHgt: NSLayoutConstraint!
    @IBOutlet weak var addonView: CustomView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Choose Product"
        
        Category.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: -(Category.imageView?.frame.size.width)!+(15), bottom: 0, right: (Category.imageView?.frame.size.width)!)
        Category.imageEdgeInsets = UIEdgeInsets.init(top: 15, left: Category.frame.size.width - 15, bottom: 15, right: 7);
        Product.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: -(Product.imageView?.frame.size.width)!+(15), bottom: 0, right: (Product.imageView?.frame.size.width)!)
        Product.imageEdgeInsets = UIEdgeInsets.init(top: 15, left: Product.frame.size.width - 15, bottom: 15, right: 7);
        
        yrf.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: -(yrf.imageView?.frame.size.width)!+(15), bottom: 0, right: (yrf.imageView?.frame.size.width)!)
        yrf.imageEdgeInsets = UIEdgeInsets.init(top: 15, left: yrf.frame.size.width - 15, bottom: 15, right: 7);
        
        eposter.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: -(eposter.imageView?.frame.size.width)!+(15), bottom: 0, right: (eposter.imageView?.frame.size.width)!)
        eposter.imageEdgeInsets = UIEdgeInsets.init(top: 15, left: eposter.frame.size.width - 15, bottom: 15, right: 7);
       
        self.collectionView.allowsMultipleSelection = true
        self.price1.textAlignment = .center
        self.price2.textAlignment = .center
        self.price3.textAlignment = .center
        self.price11.textAlignment = .center
        self.price22.textAlignment = .center
        self.price33.textAlignment = .center
        self.price111.textAlignment = .center
        self.price222.textAlignment = .center
        self.price333.textAlignment = .center
      
        self.addOnViewHgt.constant = 0
        self.addonView.isHidden = true
    
        self.proceed.isEnabled = true
       

    }
   
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        self.filterthelist()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    func filterthelist()
    {
        // Academic Category
        let searchPredicate = NSPredicate(format: "type CONTAINS[C] %@", "academic")
        academicCat = self.Productarr.filter { searchPredicate.evaluate(with: $0) } as NSArray
       
        // Bussiness Category
        let searchPredicate1 = NSPredicate(format: "type CONTAINS[C] %@", "business")
        bussinessCat = self.Productarr.filter { searchPredicate1.evaluate(with: $0) } as NSArray
        
        // Student Category
        let searchPredicate2 = NSPredicate(format: "type CONTAINS[C] %@", "student")
        studentCat = self.Productarr.filter { searchPredicate2.evaluate(with: $0) } as NSArray
        
        // Young Research Forunm
        let searchPredicate3 = NSPredicate(format: "type CONTAINS[C] %@", "yrf")
        yrfCat = self.Productarr.filter { searchPredicate3.evaluate(with: $0) } as NSArray
        
        // Eposter
        let searchPredicate4 = NSPredicate(format: "type CONTAINS[C] %@", "eitem")
        eposerCat = self.Productarr.filter { searchPredicate4.evaluate(with: $0) } as NSArray
        
        // Addon
        let searchPredicate5 = NSPredicate(format: "type CONTAINS[C] %@", "addon")
        addOnCat = self.Productarr.filter { searchPredicate5.evaluate(with: $0) } as NSArray
        self.updateFrames()
               
    }
    
    
    @IBAction func selectPinetype(_ sender:UIButton)
    {
        //21 85 135
        self.ChangeDefaultValues()
        var productname = ""
        var categotytype = ""
        if self.academicView.isHidden == false
        {
            if academicCat.count > 0
            {
                productname = ((academicCat.firstObject as! NSDictionary).value(forKey: kproductname))! as! String
                categotytype = "academic"
            }
            else
            {
                if bussinessCat.count > 0
                {
                    productname = ((bussinessCat.firstObject as! NSDictionary).value(forKey: kproductname))! as! String
                    categotytype = "business"
                }
                else if studentCat.count > 0
                {
                    productname = ((bussinessCat.firstObject as! NSDictionary).value(forKey: kproductname))! as! String
                    categotytype = "student"
                }
            }
        }
       
        switch sender.tag {
        case 200:
            gbp.backgroundColor = hexStringToUIColor(hex: "#8B812A")
            gbp.setTitleColor(hexStringToUIColor(hex: "#FFFFFF"), for: .normal)
            euro.backgroundColor = hexStringToUIColor(hex: "#FFFFFF")
            euro.setTitleColor(hexStringToUIColor(hex: "#000000"), for: .normal)
            usd.backgroundColor = hexStringToUIColor(hex: "#FFFFFF")
            usd.setTitleColor(hexStringToUIColor(hex:"#000000"), for: .normal)
            self.pannetype = "GBP"
            if productname != ""
            {
               self.updateProducts(pannetype:pannetype,productname: "\(productname)", categorytype: categotytype)
            }
            if self.yrfCat.count > 0
            {
               let productname = ((yrfCat.firstObject as! NSDictionary).value(forKey: kproductname))!
               self.updateProducts(pannetype:pannetype,productname: "\(productname)", categorytype: "yrf")
            }
            if self.eposerCat.count > 0
            {
                let productname = ((eposerCat.firstObject as! NSDictionary).value(forKey: kproductname))!
                self.updateProducts(pannetype:pannetype,productname: "\(productname)", categorytype: "eitem")
            }
            
            break
        case 201:
            gbp.backgroundColor = hexStringToUIColor(hex: "#ffffff")
            gbp.setTitleColor(hexStringToUIColor(hex: "#000000"), for: .normal)
            euro.backgroundColor = hexStringToUIColor(hex: "#8B812A")
            euro.setTitleColor(hexStringToUIColor(hex: "#ffffff"), for: .normal)
            usd.backgroundColor = hexStringToUIColor(hex: "#FFFFFF")
            usd.setTitleColor(hexStringToUIColor(hex:"#000000"), for: .normal)
            self.pannetype = "EURO"
            if productname != ""
            {
               self.updateProducts(pannetype:pannetype,productname:"\(productname)", categorytype: categotytype)
            }
            if self.yrfCat.count > 0
            {
                let productname = ((yrfCat.firstObject as! NSDictionary).value(forKey: kproductname))!
                self.updateProducts(pannetype:pannetype,productname: "\(productname)", categorytype: "yrf")
            }
            if self.eposerCat.count > 0
            {
                let productname = ((eposerCat.firstObject as! NSDictionary).value(forKey: kproductname))!
                self.updateProducts(pannetype:pannetype,productname: "\(productname)", categorytype: "eitem")
            }
            
            break
        case 202:
            gbp.backgroundColor = hexStringToUIColor(hex: "#ffffff")
            gbp.setTitleColor(hexStringToUIColor(hex: "#000000"), for: .normal)
            euro.backgroundColor = hexStringToUIColor(hex: "#FFFFFF")
            euro.setTitleColor(hexStringToUIColor(hex: "#000000"), for: .normal)
            usd.backgroundColor = hexStringToUIColor(hex: "#8B812A")
            usd.setTitleColor(hexStringToUIColor(hex:"#ffffff"), for: .normal)
            self.pannetype = "USD"
            if productname != ""
            {
               self.updateProducts(pannetype:pannetype,productname:"\(productname)", categorytype: categotytype)
            }
            if self.yrfCat.count > 0
            {
                let productname = ((yrfCat.firstObject as! NSDictionary).value(forKey: kproductname))!
                self.updateProducts(pannetype:pannetype,productname: "\(productname)", categorytype: "yrf")
            }
            if self.eposerCat.count > 0
            {
                let productname = ((eposerCat.firstObject as! NSDictionary).value(forKey: kproductname))!
                self.updateProducts(pannetype:pannetype,productname: "\(productname)", categorytype: "eitem")
            }
            break
        default:
            gbp.backgroundColor = hexStringToUIColor(hex: "#8B812A")
            gbp.setTitleColor(hexStringToUIColor(hex: "#FFFFFF"), for: .normal)
            euro.backgroundColor = hexStringToUIColor(hex: "#FFFFFF")
            euro.setTitleColor(hexStringToUIColor(hex: "#000000"), for: .normal)
            usd.backgroundColor = hexStringToUIColor(hex: "#FFFFFF")
            usd.setTitleColor(hexStringToUIColor(hex:"#000000"), for: .normal)
            self.pannetype = "GBP"
           if productname != ""
            {
               self.updateProducts(pannetype:pannetype,productname:"\(productname)", categorytype: categotytype)
            }
            if self.yrfCat.count > 0
            {
                let productname = ((yrfCat.firstObject as! NSDictionary).value(forKey: kproductname))!
                self.updateProducts(pannetype:pannetype,productname: "\(productname)", categorytype: "yrf")
            }
            if self.eposerCat.count > 0
            {
                let productname = ((eposerCat.firstObject as! NSDictionary).value(forKey: kproductname))!
                self.updateProducts(pannetype:pannetype,productname: "\(productname)", categorytype: "eitem")
            }
            break
        }
    }
    func updateFrames()
    {
        if self.academicCat.count == 0
        {
            DispatchQueue.main.async {
                self.academicView.isHidden = true
                self.academicViewHgt.constant = 0
            }
        }
        if yrfCat.count == 0
        {
            DispatchQueue.main.async {
                self.yrfView.isHidden = true
                self.yrfViewHgt.constant = 0
            }
        }
        if eposerCat.count == 0
        {
            DispatchQueue.main.async {
                self.eposterView.isHidden = true
                self.eposterViewHgt.constant = 0
            }
        }
        if self.academicCat.count > 0
        {
           self.Categoryfields.append("Academic")
           self.getPackages(field: "academic")
        }
        if self.bussinessCat.count > 0
        {
            self.Categoryfields.append("Business")
        }
        if self.studentCat.count > 0
        {
            self.Categoryfields.append("Student")
        }
        
        if yrfCat.count > 0
        {
            self.getPackages(field: "yrf")
        }
        if eposerCat.count > 0
        {
            self.getPackages(field: "eitem")
        }
        
        if addOnCat.count > 0
        {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
        
    }
    func updateProducts(pannetype:String,productname:String,categorytype:String)
    {
        // Only Registration
        
       // self.ChangeDefaultValues()
        var registration = NSArray()
        let searchPredicate = NSPredicate(format: "productname CONTAINS[C] %@", productname)
       
        if categorytype == "academic"
        {
            registration = self.academicCat.filter { searchPredicate.evaluate(with: $0) } as NSArray
            self.Category.setTitle(categorytype, for: .normal)
            self.Product.setTitle(productname, for: .normal)
        }
        else if categorytype == "business"
        {
            registration = self.bussinessCat.filter { searchPredicate.evaluate(with: $0) } as NSArray
            self.Category.setTitle(categorytype, for: .normal)
            self.Product.setTitle(productname, for: .normal)
        }
        else if categorytype == "student"
        {
            registration = self.studentCat.filter { searchPredicate.evaluate(with: $0) } as NSArray
            self.Category.setTitle(categorytype, for: .normal)
            self.Product.setTitle(productname, for: .normal)
        }
        else if categorytype == "yrf"
        {
            registration = self.yrfCat.filter { searchPredicate.evaluate(with: $0) } as NSArray
            self.yrf.setTitle(productname, for: .normal)
        }
        else if categorytype == "eitem"
        {
            registration = self.eposerCat.filter { searchPredicate.evaluate(with: $0) } as NSArray
            self.eposter.setTitle(productname, for: .normal)
        }
         
        
        if registration.count > 0
        {
            let dictin = (registration.firstObject as! NSDictionary)
            if categorytype == "yrf"
            {
                self.early1.text = "On/Before \n \(dictin.value(forKey: "early")!)"
                self.normal1.text = "On/Before \n \(dictin.value(forKey: "normal")!)"
                self.final1.text = "On/Before \n \(dictin.value(forKey: "final")!)"
             
                switch pannetype {
                case "GBP":
                    self.price11.attributedText = getattributedText(date:"\(dictin.value(forKey: "early")!)",price:"£\(dictin.value(forKey: "price7")!)", isfinal: false)
                    self.price22.attributedText = getattributedText(date:"\(dictin.value(forKey: "normal")!)",price:"£\(dictin.value(forKey: "price8")!)", isfinal: false)
                    self.price33.attributedText = getattributedText(date:"\(dictin.value(forKey: "final")!)",price:"£\(dictin.value(forKey: "price9")!)", isfinal: true)
                    self.totalPrice.text = "£\(self.totalProdPrice)"
                    break
                case "EURO":
                    self.price11.attributedText = getattributedText(date:"\(dictin.value(forKey: "early")!)",price:"€\(dictin.value(forKey: "price4")!)", isfinal: false)
                    self.price22.attributedText = getattributedText(date:"\(dictin.value(forKey: "normal")!)",price:"€\(dictin.value(forKey: "price5")!)", isfinal: false)
                    self.price33.attributedText = getattributedText(date:"\(dictin.value(forKey: "final")!)",price:"€\(dictin.value(forKey: "price6")!)", isfinal: true)
                    self.totalPrice.text = "€\(self.totalProdPrice)"
                    break
                case "USD":
                    self.price11.attributedText = getattributedText(date:"\(dictin.value(forKey: "early")!)",price:"$\(dictin.value(forKey: "price1")!)", isfinal: false)
                    self.price22.attributedText = getattributedText(date:"\(dictin.value(forKey: "normal")!)",price:"$\(dictin.value(forKey: "price2")!)", isfinal: false)
                    self.price33.attributedText = getattributedText(date:"\(dictin.value(forKey: "final")!)",price:"$\(dictin.value(forKey: "price3")!)", isfinal: true)
                    self.totalPrice.text = "$\(self.totalProdPrice)"
                    break
                default:
                    break
                }
            }
            else if categorytype == "eitem"
            {
                self.early2.text = "On/Before \n \(dictin.value(forKey: "early")!)"
                self.normal2.text = "On/Before \n \(dictin.value(forKey: "normal")!)"
                self.final2.text = "On/Before \n \(dictin.value(forKey: "final")!)"
                switch pannetype {
                case "GBP":
                    self.price111.attributedText = getattributedText(date:"\(dictin.value(forKey: "early")!)",price:"£\(dictin.value(forKey: "price7")!)", isfinal: false)
                    self.price222.attributedText = getattributedText(date:"\(dictin.value(forKey: "normal")!)",price:"£\(dictin.value(forKey: "price8")!)", isfinal: false)
                    self.price333.attributedText = getattributedText(date:"\(dictin.value(forKey: "final")!)",price:"£\(dictin.value(forKey: "price9")!)", isfinal: true)
                    self.totalPrice.text = "£\(self.totalProdPrice)"
                    break
                case "EURO":
                    self.price111.attributedText = getattributedText(date:"\(dictin.value(forKey: "early")!)",price:"€\(dictin.value(forKey: "price4")!)", isfinal: false)
                    self.price222.attributedText = getattributedText(date:"\(dictin.value(forKey: "normal")!)",price:"€\(dictin.value(forKey: "price5")!)", isfinal: false)
                    self.price333.attributedText = getattributedText(date:"\(dictin.value(forKey: "final")!)",price:"€\(dictin.value(forKey: "price6")!)", isfinal: true)
                    self.totalPrice.text = "€\(self.totalProdPrice)"
                    break
                case "USD":
                 
                    self.price111.attributedText = getattributedText(date:"\(dictin.value(forKey: "early")!)",price:"$\(dictin.value(forKey: "price1")!)", isfinal: false)
                    self.price222.attributedText = getattributedText(date:"\(dictin.value(forKey: "normal")!)",price:"$\(dictin.value(forKey: "price2")!)", isfinal: false)
                    self.price333.attributedText = getattributedText(date:"\(dictin.value(forKey: "final")!)",price:"$\(dictin.value(forKey: "price3")!)", isfinal: true)
                    self.totalPrice.text = "$\(self.totalProdPrice)"
                    break
                default:
                    break
                }
            }
            else
            {
            self.early.text = "On/Before \n \(dictin.value(forKey: "early")!)"
            self.normal.text = "On/Before \n \(dictin.value(forKey: "normal")!)"
            self.final.text = "On/Before \n \(dictin.value(forKey: "final")!)"
            switch pannetype {
            case "GBP":
                
                self.price1.attributedText = getattributedText(date:"\(dictin.value(forKey: "early")!)",price:"£\(dictin.value(forKey: "price7")!)", isfinal: false)
                self.price2.attributedText = getattributedText(date:"\(dictin.value(forKey: "normal")!)",price:"£\(dictin.value(forKey: "price8")!)", isfinal: false)
                self.price3.attributedText = getattributedText(date:"\(dictin.value(forKey: "final")!)",price:"£\(dictin.value(forKey: "price9")!)", isfinal: true)
                self.totalPrice.text = "£\(self.totalProdPrice)"
                break
            case "EURO":
            
                self.price1.attributedText = getattributedText(date:"\(dictin.value(forKey: "early")!)",price:"€\(dictin.value(forKey: "price4")!)", isfinal: false)
                self.price2.attributedText = getattributedText(date:"\(dictin.value(forKey: "normal")!)",price:"€\(dictin.value(forKey: "price5")!)", isfinal: false)
                self.price3.attributedText = getattributedText(date:"\(dictin.value(forKey: "final")!)",price:"€\(dictin.value(forKey: "price6")!)", isfinal: true)
                self.totalPrice.text = "€\(self.totalProdPrice)"
                
                break
            case "USD":
                self.price1.attributedText = getattributedText(date:"\(dictin.value(forKey: "early")!)",price:"$\(dictin.value(forKey: "price1")!)", isfinal: false)
                self.price2.attributedText = getattributedText(date:"\(dictin.value(forKey: "normal")!)",price:"$\(dictin.value(forKey: "price2")!)", isfinal: false)
                self.price3.attributedText = getattributedText(date:"\(dictin.value(forKey: "final")!)",price:"$\(dictin.value(forKey: "price3")!)", isfinal: true)
                self.totalPrice.text = "$\(self.totalProdPrice)"
                break
            default:
                break
            }
            }
            DispatchQueue.main.async {
                self.addOnViewHgt.constant = 0
                self.addonView.isHidden = true
            }
    }
    }
    func ChangeDefaultValues(){
        
        self.price11.layer.borderWidth = 0.0
        self.price22.layer.borderWidth = 0.0
        self.price33.layer.borderWidth = 0.0
        self.price111.layer.borderWidth = 0.0
        self.price222.layer.borderWidth = 0.0
        self.price333.layer.borderWidth = 0.0
        self.price1.layer.borderWidth = 0.0
        self.price2.layer.borderWidth = 0.0
        self.price3.layer.borderWidth = 0.0
        self.price1.textColor = hexStringToUIColor(hex: kdefalutcolor)
        self.price2.textColor = hexStringToUIColor(hex: kdefalutcolor)
        self.price3.textColor = hexStringToUIColor(hex: kdefalutcolor)
        self.price11.textColor = hexStringToUIColor(hex: kdefalutcolor)
        self.price22.textColor = hexStringToUIColor(hex: kdefalutcolor)
        self.price33.textColor = hexStringToUIColor(hex: kdefalutcolor)
        self.price111.textColor = hexStringToUIColor(hex: kdefalutcolor)
        self.price222.textColor = hexStringToUIColor(hex: kdefalutcolor)
        self.price333.textColor = hexStringToUIColor(hex: kdefalutcolor)
        Product1.setImage(imageUnchecked, for: .normal)
        Product2.setImage(imageUnchecked, for: .normal)
        Product3.setImage(imageUnchecked, for: .normal)
        self.totalProdPrice = 0
        self.addOntotal = 0
        self.Product1.tag = 50
        self.Product2.tag = 60
        self.Product3.tag = 70
        self.prods.removeAll()
        self.addonprods.removeAll()
    }
    @IBAction func Categorytype(_ sender: UIButton)
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let viewTobeLoad = storyBoard.instantiateViewController(withIdentifier: "Country") as! SelectCountryVC
        viewTobeLoad.delegate1 = self
        viewTobeLoad.iscountry = false
        viewTobeLoad.fields = self.Categoryfields
        self.setDefaults(type:"category")
        self.navigationController?.present(viewTobeLoad, animated: true, completion: nil)
    }
    func setDefaults(type:String)
    {
        if type == "category"
        {
            self.price1.layer.borderWidth = 0.0
            self.price2.layer.borderWidth = 0.0
            self.price3.layer.borderWidth = 0.0
            self.price1.textColor = hexStringToUIColor(hex: kdefalutcolor)
            self.price2.textColor = hexStringToUIColor(hex: kdefalutcolor)
            self.price3.textColor = hexStringToUIColor(hex: kdefalutcolor)
            self.cleartheProduct(type:"Category1", price: selectprice)
        }
        
        if type == "yrf"
        {
            self.price11.layer.borderWidth = 0.0
            self.price22.layer.borderWidth = 0.0
            self.price33.layer.borderWidth = 0.0
            self.price11.textColor = hexStringToUIColor(hex: kdefalutcolor)
            self.price22.textColor = hexStringToUIColor(hex: kdefalutcolor)
            self.price33.textColor = hexStringToUIColor(hex: kdefalutcolor)
            self.cleartheProduct(type:"Category2", price: selectprice1)
        }
        if type == "eitem"
       {
           self.price111.layer.borderWidth = 0.0
           self.price222.layer.borderWidth = 0.0
           self.price333.layer.borderWidth = 0.0
           self.price111.textColor = hexStringToUIColor(hex: kdefalutcolor)
           self.price222.textColor = hexStringToUIColor(hex: kdefalutcolor)
           self.price333.textColor = hexStringToUIColor(hex: kdefalutcolor)
           self.cleartheProduct(type:"Category3", price: selectprice2)
       }
        
    }
    @IBAction func Packagetype(_ sender: UIButton)
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let viewTobeLoad = storyBoard.instantiateViewController(withIdentifier: "Country") as! SelectCountryVC
        viewTobeLoad.delegate1 = self
        viewTobeLoad.iscountry = false
        viewTobeLoad.iscategory = false
        
        if sender.tag == 100
        {
            viewTobeLoad.isyrf = true
            self.setDefaults(type:"yrf")
            self.getPackages(field: "yrf")
        }
        else if sender.tag == 200
        {
            viewTobeLoad.iseposter = true
            self.setDefaults(type:"eitem")
            self.getPackages(field: "eitem")
        }
        else
        {
            self.setDefaults(type:"category")
            self.getPackages(field: (self.Category.currentTitle!).lowercased())
        }
        viewTobeLoad.fields = self.packagenamearr
        self.navigationController?.present(viewTobeLoad, animated: true, completion: nil)
    }
    
    // Attributed String
    func getattributedText(date:String,price:String,isfinal:Bool)-> NSMutableAttributedString
    {
        let attr = NSMutableAttributedString(string: price)
        let earlydate = date
        let earlyresult = self.dateformatter(datestr: earlydate)
       
        let obj = UtilityManager(dateService: dateUtility!)
        let currentdatestr = obj.getCurrentDate()
        let currentdate = self.dateformatter(datestr: currentdatestr)
       
//        if date1 != nil
//        {
        switch earlyresult.compare(currentdate) {
            case .orderedAscending:
                print("date1:\(earlydate)&&currentdate:\(currentdate) is order ascending")
                if isfinal
                  {
                  attr.addAttribute(NSAttributedString.Key.font, value: UIFont(name: kAppFontBold, size: 12.0)!, range: NSMakeRange(0, price.count))
                  attr.addAttribute(NSAttributedString.Key.foregroundColor, value: hexStringToUIColor(hex: kdefalutcolor), range:NSMakeRange(0, price.count))
                  return attr
                }
                attr.addAttribute(NSAttributedString.Key.font, value: UIFont(name: kAppFontBold, size: 12.0)!, range: NSMakeRange(0, price.count))
                        attr.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.darkGray, range:NSMakeRange(0, price.count))
                        attr.addAttribute(NSAttributedString.Key.strikethroughColor, value:UIColor.darkGray, range: NSMakeRange(0, price.count))
                        attr.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSNumber(value: NSUnderlineStyle.single.rawValue), range: NSMakeRange(0, price.count))
                  
                
                        return attr
                
            case .orderedDescending:
                print("date1:\(earlydate)&&currentdate:\(currentdate) is order descending")
                attr.addAttribute(NSAttributedString.Key.font, value: UIFont(name: kAppFontBold, size: 12.0)!, range: NSMakeRange(0, price.count))
                attr.addAttribute(NSAttributedString.Key.foregroundColor, value: hexStringToUIColor(hex: kdefalutcolor), range:NSMakeRange(0, price.count))
                return attr
                
                
            case .orderedSame:
                print("date1:\(earlydate)&&currentdate:\(currentdate) is order same")
                attr.addAttribute(NSAttributedString.Key.font, value: UIFont(name: kAppFontBold, size: 12.0)!, range: NSMakeRange(0, price.count))
                attr.addAttribute(NSAttributedString.Key.foregroundColor, value: hexStringToUIColor(hex: kdefalutcolor), range:NSMakeRange(0, price.count))
                return attr
                
            default:
                
            break
            }
        
           /* if ((date1?.compare(currentdate))?.rawValue)! < 0
            {
                attr.addAttribute(NSAttributedString.Key.font, value: UIFont(name: kAppFontBold, size: 12.0)!, range: NSMakeRange(0, price.count))
                attr.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.darkGray, range:NSMakeRange(0, price.count))
                attr.addAttribute(NSAttributedString.Key.strikethroughColor, value:UIColor.darkGray, range: NSMakeRange(0, price.count))
                attr.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSNumber(value: NSUnderlineStyle.single.rawValue), range: NSMakeRange(0, price.count))
        
                return attr
            }
            else
            {
                attr.addAttribute(NSAttributedString.Key.font, value: UIFont(name: kAppFontBold, size: 12.0)!, range: NSMakeRange(0, price.count))
                attr.addAttribute(NSAttributedString.Key.foregroundColor, value: hexStringToUIColor(hex: kdefalutcolor), range:NSMakeRange(0, price.count))
                
                return attr
            }*/
     //   }
//        else
//        {
//           return NSMutableAttributedString(string: price)
//        }
        
       return attr
    }
    func dateformatter(datestr:String)-> Date
    {
        var str = datestr
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = kRequiredDateFormat
        let str1 = str.split(separator: " ")
        let firstindex = str1.first
        
        if firstindex!.count >= 3
            {
                let firstsubstr = firstindex?.prefix(3)
                let str2 = firstsubstr
                var str3 = String()
                if str1.count == 3
                {
                    str = str2! + " " + str1[1] + " " + str1[2]
                    print(str)
                }
                if str1.count == 2
                {
                    if !((str1[1].contains(" ")))
                    {
                      var newText = String()
                      let x = str1[1]
                      for (_, character) in x.enumerated() {
                          if character == "," {
                              newText.append(character)
                              newText.append(" ")
                          }
                          else
                          {
                               newText.append(character)
                          }
                      }
                      
                      str3 = newText
                      str = str2! + " " + str3
                    }
                    else
                    {
                        str = str2! + " " + str1[1]
                        
                    }
                }
               
                print(str)
            }
        var dateval = dateFormatter.date(from: str)
        if dateval == nil
        {
           dateval = self.dateformatter(datestr: str)
        }
        return dateval!
    }
   
    func sendnamedfields(fieldname: String,iscategoty:Bool,isyrf:Bool,iseposter:Bool) {
        dismiss(animated: true, completion: nil)
        if iscategoty == true
        {
            self.Category.setTitle(fieldname, for: .normal)
            self.getPackages(field:fieldname.lowercased())
        }
        else
        {
            if isyrf == true
            {
                self.yrf.setTitle(fieldname, for: .normal)
                self.updateProducts(pannetype: self.pannetype, productname: fieldname, categorytype: "yrf")
            }
            else if iseposter == true
            {
                self.eposter.setTitle(fieldname, for: .normal)
                self.updateProducts(pannetype: self.pannetype, productname: fieldname, categorytype: "eitem")
            }
            else
            {
                self.Product.setTitle(fieldname, for: .normal)
                self.updateProducts(pannetype: self.pannetype, productname: fieldname, categorytype: (self.Category.currentTitle!).lowercased())
            }
        }
    }
    
    func getPackages(field:String)
    {
        self.packagenamearr.removeAll()
        if(field == "academic")
        {
            if academicCat.count > 0
            {
                for dict in academicCat
                {
                    let dictin = dict as? NSDictionary ?? [:]
                    if dictin.count > 0
                    {
                        let productname = iskeyexist(dict: dictin, key: kproductname)
                        if productname != ""
                        {
                            self.packagenamearr.append(productname)
                        }
                    }
                }
            }
        }
        else if(field == "business")
        {
            if bussinessCat.count > 0
            {
                for dict in bussinessCat
                {
                    let dictin = dict as? NSDictionary ?? [:]
                    if dictin.count > 0
                    {
                        let productname = iskeyexist(dict: dictin, key: kproductname)
                        if productname != ""
                        {
                            self.packagenamearr.append(productname)
                        }
                    }
                }
            }
        }
        else if(field == "student")
               {
                   if studentCat.count > 0
                   {
                    for dict in studentCat
                    {
                        let dictin = dict as? NSDictionary ?? [:]
                        if dictin.count > 0
                        {
                            let productname = iskeyexist(dict: dictin, key: kproductname)
                            if productname != ""
                            {
                                self.packagenamearr.append(productname)
                            }
                        }
                    }
                   }
               }
        else if(field == "yrf")
        {
            if yrfCat.count > 0
            {
                for dict in yrfCat
                {
                    let dictin = dict as? NSDictionary ?? [:]
                    if dictin.count > 0
                    {
                        let productname = iskeyexist(dict: dictin, key: kproductname)
                        if productname != ""
                        {
                            self.packagenamearr.append(productname)
                        }
                    }
                }
            }
        }
        else if(field == "eitem")
              {
                  if eposerCat.count > 0
                  {
                    for dict in eposerCat
                    {
                        let dictin = dict as? NSDictionary ?? [:]
                        if dictin.count > 0
                        {
                            let productname = iskeyexist(dict: dictin, key: kproductname)
                            if productname != ""
                            {
                                self.packagenamearr.append(productname)
                            }
                        }
                    }
                  }
              }
         if field == "yrf"
         {
            if self.packagenamearr.count > 0
            {
                self.yrf.setTitle(packagenamearr.first!, for: .normal)
                          self.updateProducts(pannetype: self.pannetype, productname: packagenamearr.first!, categorytype: "yrf")
            }
          
         }
         else if field == "eitem"
         {
            if self.packagenamearr.count > 0
            {
                self.eposter.setTitle(packagenamearr.first!, for: .normal)
                self.updateProducts(pannetype: self.pannetype, productname: packagenamearr.first!, categorytype: "eitem")
            }
            
         }
        else
         {
            if self.packagenamearr.count > 0
            {
                self.Product.setTitle(packagenamearr.first!, for: .normal)
                self.updateProducts(pannetype: self.pannetype, productname: packagenamearr.first!, categorytype: (self.Category.currentTitle!).lowercased())
            }
            
         }
    }
    //MARK:- Buy the Product
    @IBAction func buytheProduct(_ sender:UIButton)
    {
        
        switch sender.tag {
            
        // Category Products
        case 50:
            
            Product1.setImage(imageChecked, for: .normal)
            Product1.tag = 51
            self.proceed.isEnabled = true
            let values = self.getProductPrice(category: (self.Category.currentTitle!).lowercased(), package: self.Product.currentTitle!)
            index = values.index
            selectprice = values.price
           
            let regProduct_id = values.dict.value(forKey: "regproducts_id")
            let dict = NSMutableDictionary()
            dict.setValue(self.Category.currentTitle, forKey: "Category1")
            dict.setValue(self.Product.currentTitle, forKey: "Package1")
            dict.setValue(Int(selectprice.dropFirst()), forKey: "Price")
            dict.setValue(regProduct_id, forKey: "regproducts_id")
            dict.setValue(self.Category.currentTitle, forKey: "title")
            
            self.prods.append(dict)
                       
            if self.addOnCat.count > 0
            {
                DispatchQueue.main.async {
                    self.addOnViewHgt.constant = 155
                    self.addonView.isHidden = false
                    //self.scrollView.setContentOffset(CGPoint(x: 0, y: max(self.scrollView.contentSize.height, self.scrollView.bounds.size.height)), animated: false)
                    self.collectionView.reloadData()
                }
            }
            else
            {
                DispatchQueue.main.async {
                    self.addOnViewHgt.constant = 0
                    self.addonView.isHidden = true
                }
            }
            self.updateTotalPrice(price:selectprice,type:"add")
            break
        case 51:
            self.cleartheProduct(type:"Category1",price:selectprice)
            Product1.setImage(imageUnchecked, for: .normal)
            Product1.tag = 50
            self.proceed.isEnabled = false
            self.removeSelection(index:index,price:selectprice)
            DispatchQueue.main.async {
                self.addOnViewHgt.constant = 0
                self.addonView.isHidden = true
                self.collectionView.reloadData()
            }
            //self.updateTotalPrice(price:selectprice,type:"minus")
            break
            
        // YRF Products
        case 60:
            Product2.setImage(imageChecked, for: .normal)
            Product2.tag = 61
            let values = self.getProductPrice(category: "yrf", package: self.yrf.currentTitle!)
            index1 = values.index
            selectprice1 = values.price
            let regProduct_id = values.dict.value(forKey: "regproducts_id")
            let dict = NSMutableDictionary()
            dict.setValue("yrf", forKey: "Category2")
            dict.setValue(self.yrf.currentTitle, forKey: "Package2")
            dict.setValue(Int(selectprice1.dropFirst()), forKey: "Price")
            dict.setValue(regProduct_id, forKey: "regproducts_id")
            dict.setValue("yrf", forKey: "title")
            self.prods.append(dict)
            self.updateTotalPrice(price:selectprice1,type:"add")
            break
        case 61:
            self.cleartheProduct(type:"Category2",price:selectprice1)
            Product2.setImage(imageUnchecked, for: .normal)
            Product2.tag = 60
            removeSelection1(index: index1, price: selectprice1)
            
           // self.updateTotalPrice(price:selectprice1,type:"minus")
            break
            
        // EPoster Products
        case 70:
            Product3.setImage(imageChecked, for: .normal)
            Product3.tag = 71
            let values = self.getProductPrice(category: "eitem", package: self.eposter.currentTitle!)
            index2 = values.index
            selectprice2 = values.price
            let regProduct_id = values.dict.value(forKey: "regproducts_id")
            let dict = NSMutableDictionary()
            dict.setValue("eitem", forKey: "Category3")
            dict.setValue(self.eposter.currentTitle, forKey: "Package3")
            dict.setValue(Int(selectprice2.dropFirst()), forKey: "Price")
            dict.setValue(regProduct_id, forKey: "regproducts_id")
            self.prods.append(dict)
            self.updateTotalPrice(price:selectprice2,type:"add")
            break
        case 71:
              self.cleartheProduct(type:"Category3",price:selectprice2)
              Product3.setImage(imageUnchecked, for: .normal)
              Product3.tag = 70
              removeSelection2(index: index2, price: selectprice2)
              
            //  self.updateTotalPrice(price:selectprice2,type:"minus")
              break
        default:
            Product1.setImage(imageUnchecked, for: .normal)
            Product2.setImage(imageUnchecked, for: .normal)
            Product3.setImage(imageUnchecked, for: .normal)
            break
        }
    }
    func cleartheProduct(type:String,price:String)
    {
        var clearProd = self.prods
        
        var i = 0
        if (self.prods.count > 0 && i < self.prods.count)
        {
            for dict in clearProd
            {
                for (key, _) in dict
                {
                    let keyvalue = key as! String
                    if keyvalue == type
                    {
                        clearProd.remove(at: i)
                        if type == "Category1"
                        {
                            if Product1.tag == 51
                               {
                                   self.Product1.tag = 50
                                   self.Product1.setImage(imageUnchecked, for: .normal)
                                   self.totalProdPrice = self.totalProdPrice - self.addOntotal
                                   self.updateTotalPrice(price: selectprice, type: "minus")
                               }
                        }
                        if type == "Category2"
                        {
                            if Product2.tag == 61
                            {
                               self.Product2.tag = 60
                               self.Product2.setImage(imageUnchecked, for: .normal)
                                self.updateTotalPrice(price: selectprice1, type: "minus")
                            }
                        }
                        if type == "Category3"
                        {
                            if Product3.tag == 71
                               {
                                  self.Product3.tag = 70
                                  self.Product3.setImage(imageUnchecked, for: .normal)
                                   self.updateTotalPrice(price: selectprice2, type: "minus")
                               }
                        }
                    }
                }
                i = i+1
            }
        }
        self.prods = clearProd
    }
    func updateTotalPrice(price:String,type:String)
    {
        let cost = price.dropFirst()
        if type == "add"
        {
            totalProdPrice = totalProdPrice + Int(cost)!
        }
        if type == "minus"
        {
            totalProdPrice = totalProdPrice - Int(cost)!
        }
        if pannetype == "GBP"
        {
            self.totalPrice.text = "£\(totalProdPrice)"
        }
        if pannetype == "EURO"
        {
            self.totalPrice.text = "€\(totalProdPrice)"
        }
        
        if pannetype == "USD"
        {
            self.totalPrice.text = "$\(totalProdPrice)"
        }
        
    }
    func getProductPrice(category:String,package:String)->(price:String,index:Int,dict:NSDictionary)
    {
           var registration = NSArray()
           var status = [String]()
           let searchPredicate = NSPredicate(format: "productname CONTAINS[C] %@", package)
           if category == "academic"
           {
               registration = self.academicCat.filter { searchPredicate.evaluate(with: $0) } as NSArray
           }
           else if category == "business"
           {
               registration = self.bussinessCat.filter { searchPredicate.evaluate(with: $0) } as NSArray
           }
           else if category == "student"
           {
               registration = self.studentCat.filter { searchPredicate.evaluate(with: $0) } as NSArray
           }
           else if category == "yrf"
           {
               registration = self.yrfCat.filter { searchPredicate.evaluate(with: $0) } as NSArray
           }
           else if category == "eitem"
           {
               registration = self.eposerCat.filter { searchPredicate.evaluate(with: $0) } as NSArray
           }
        if registration.count > 0
        {
            let dictin = registration.firstObject as? NSDictionary ?? [:]
            if dictin.count > 0
            {
                      let earlydate = iskeyexist(dict: dictin, key: "early")
                      let obj = UtilityManager(dateService: dateUtility!)
                      let formatteddate = self.dateformatter(datestr: earlydate)
                      let currentdate = self.dateformatter(datestr: obj.getCurrentDate())
                     // currentdate = Calendar.current.date(byAdding: .day, value: 50, to: currentdate!)
                      if currentdate != nil && formatteddate != nil
                      {
                        if ((formatteddate.compare(currentdate)).rawValue < 0)
                           {
                                status.append(kdateExpired)
                           }
                           else
                           {
                                status.append(kdateavailble)
                           }
                     }
                           let normaldate = iskeyexist(dict: dictin, key: "normal")
                      let formatteddate1 = self.dateformatter(datestr: normaldate)
                     if currentdate != nil && formatteddate != nil
                     {
                        if (formatteddate1.compare(currentdate)).rawValue < 0
                        {
                           status.append(kdateExpired)
                        }
                        else
                        {
                           status.append(kdateavailble)
                        }
                     }
                           
                           let finaldate = iskeyexist(dict: dictin, key: "final")
                let formatteddate2 = self.dateformatter(datestr: finaldate)
                if currentdate != nil && formatteddate2 != nil
                {
                    /*if (formatteddate2.compare(currentdate)).rawValue < 0
                      {
                         status.append(kdateExpired)
                      }
                      else
                      {
                         status.append(kdateavailble)
                      }*/
                    status.append(kdateavailble)
                }
                          
                        if category == "yrf"
                        {
                            let prices = getStatusUpdate1(status: status)
                            return (prices.price,prices.index,dictin)
                        }
                        if category == "academic" || category == "business" || category == "student"
                        {
                            let prices = getStatusUpdate(status: status)
                            return (prices.price,prices.index,dictin)
                        }
                        if category == "eitem"
                        {
                            let prices = getStatusUpdate2(status: status)
                            return (prices.price,prices.index,dictin)
                        }
                      
            }
             return("",0,dictin)
        }
        else
        {
            return("",0,[:])
        }
        
    }
    func getStatusUpdate(status:[String]) -> (price:String,tag:Int,index:Int)
    {
        for (index,item) in status.enumerated()
        {
            if item == kdateavailble
            {
                switch index {
                case 0:
                    let price = self.price1.text
                    let attr = NSMutableAttributedString(string: price!)
                    attr.addAttribute(NSAttributedString.Key.font, value: UIFont(name: kAppFontBold, size: 12.0)!, range: NSMakeRange(0, price!.count))
                    attr.addAttribute(NSAttributedString.Key.foregroundColor, value: hexStringToUIColor(hex: kselectionColor), range:NSMakeRange(0, price!.count))
                    self.price1.layer.borderColor = hexStringToUIColor(hex: kselectionColor).cgColor
                    self.price1.layer.borderWidth = 1.0
                    self.price1.layer.cornerRadius = 2.0
                    self.price1.attributedText = attr
                    return (self.price1.text!,self.price1.tag,index:index)
                case 1:
                    let price = self.price2.text
                    let attr = NSMutableAttributedString(string: price!)
                    attr.addAttribute(NSAttributedString.Key.font, value: UIFont(name: kAppFontBold, size: 12.0)!, range: NSMakeRange(0, price!.count))
                    attr.addAttribute(NSAttributedString.Key.foregroundColor, value: hexStringToUIColor(hex: kselectionColor), range:NSMakeRange(0, price!.count))
                    self.price2.layer.borderColor = hexStringToUIColor(hex: kselectionColor).cgColor
                    self.price2.layer.borderWidth = 1.0
                    self.price2.layer.cornerRadius = 2.0
                    self.price2.attributedText = attr
                    return (self.price2.text!,self.price2.tag,index:index)
                case 2:
                    let price = self.price3.text
                    let attr = NSMutableAttributedString(string: price!)
                    attr.addAttribute(NSAttributedString.Key.font, value: UIFont(name: kAppFontBold, size: 12.0)!, range: NSMakeRange(0, price!.count))
                    attr.addAttribute(NSAttributedString.Key.foregroundColor, value: hexStringToUIColor(hex: kselectionColor), range:NSMakeRange(0, price!.count))
                    self.price3.layer.borderColor = hexStringToUIColor(hex: kselectionColor).cgColor
                    self.price3.layer.borderWidth = 1.0
                    self.price3.layer.cornerRadius = 2.0
                    self.price3.attributedText = attr
                    return (self.price3.text!,self.price3.tag,index:index)
                default:
                    break
                }
            }
        }
        return ("",0,0)
    }
    func getStatusUpdate1(status:[String]) -> (price:String,tag:Int,index:Int)
    {
        for (index,item) in status.enumerated()
        {
            if item == kdateavailble
            {
                switch index {
                case 0:
                    let price = self.price11.text
                    let attr = NSMutableAttributedString(string: price!)
                    attr.addAttribute(NSAttributedString.Key.font, value: UIFont(name: kAppFontBold, size: 12.0)!, range: NSMakeRange(0, price!.count))
                    attr.addAttribute(NSAttributedString.Key.foregroundColor, value: hexStringToUIColor(hex: kselectionColor), range:NSMakeRange(0, price!.count))
                    self.price11.layer.borderColor = hexStringToUIColor(hex: kselectionColor).cgColor
                    self.price11.layer.borderWidth = 1.0
                    self.price11.layer.cornerRadius = 2.0
                    self.price11.attributedText = attr
                    return (self.price11.text!,self.price11.tag,index:index)
                case 1:
                    let price = self.price22.text
                    let attr = NSMutableAttributedString(string: price!)
                    attr.addAttribute(NSAttributedString.Key.font, value: UIFont(name: kAppFontBold, size: 12.0)!, range: NSMakeRange(0, price!.count))
                    attr.addAttribute(NSAttributedString.Key.foregroundColor, value: hexStringToUIColor(hex: kselectionColor), range:NSMakeRange(0, price!.count))
                    self.price22.layer.borderColor = hexStringToUIColor(hex: kselectionColor).cgColor
                    self.price22.layer.borderWidth = 1.0
                    self.price22.layer.cornerRadius = 2.0
                    self.price22.attributedText = attr
                    return (self.price22.text!,self.price22.tag,index:index)
                case 2:
                    let price = self.price33.text
                    let attr = NSMutableAttributedString(string: price!)
                    attr.addAttribute(NSAttributedString.Key.font, value: UIFont(name: kAppFontBold, size: 12.0)!, range: NSMakeRange(0, price!.count))
                    attr.addAttribute(NSAttributedString.Key.foregroundColor, value: hexStringToUIColor(hex: kselectionColor), range:NSMakeRange(0, price!.count))
                    self.price33.layer.borderColor = hexStringToUIColor(hex: kselectionColor).cgColor
                    self.price33.layer.borderWidth = 1.0
                    self.price33.layer.cornerRadius = 2.0
                    self.price33.attributedText = attr
                    return (self.price33.text!,self.price33.tag,index:index)
                default:
                    break
                }
            }
        }
        return ("",0,0)
    }
    func getStatusUpdate2(status:[String]) -> (price:String,tag:Int,index:Int)
    {
        for (index,item) in status.enumerated()
        {
            if item == kdateavailble
            {
                switch index {
                case 0:
                    let price = self.price111.text
                    let attr = NSMutableAttributedString(string: price!)
                    attr.addAttribute(NSAttributedString.Key.font, value: UIFont(name: kAppFontBold, size: 12.0)!, range: NSMakeRange(0, price!.count))
                    attr.addAttribute(NSAttributedString.Key.foregroundColor, value: hexStringToUIColor(hex: kselectionColor), range:NSMakeRange(0, price!.count))
                    self.price111.layer.borderColor = hexStringToUIColor(hex: kselectionColor).cgColor
                    self.price111.layer.borderWidth = 1.0
                    self.price111.layer.cornerRadius = 2.0
                    self.price111.attributedText = attr
                    return (self.price111.text!,self.price111.tag,index:index)
                case 1:
                    let price = self.price222.text
                    let attr = NSMutableAttributedString(string: price!)
                    attr.addAttribute(NSAttributedString.Key.font, value: UIFont(name: kAppFontBold, size: 12.0)!, range: NSMakeRange(0, price!.count))
                    attr.addAttribute(NSAttributedString.Key.foregroundColor, value: hexStringToUIColor(hex: kselectionColor), range:NSMakeRange(0, price!.count))
                    self.price222.layer.borderColor = hexStringToUIColor(hex: kselectionColor).cgColor
                    self.price222.layer.borderWidth = 1.0
                    self.price222.layer.cornerRadius = 2.0
                    self.price222.attributedText = attr
                    return (self.price222.text!,self.price222.tag,index:index)
                case 2:
                    let price = self.price333.text
                    let attr = NSMutableAttributedString(string: price!)
                    attr.addAttribute(NSAttributedString.Key.font, value: UIFont(name: kAppFontBold, size: 12.0)!, range: NSMakeRange(0, price!.count))
                    attr.addAttribute(NSAttributedString.Key.foregroundColor, value: hexStringToUIColor(hex: kselectionColor), range:NSMakeRange(0, price!.count))
                    self.price333.layer.borderColor = hexStringToUIColor(hex: kselectionColor).cgColor
                    self.price333.layer.borderWidth = 1.0
                    self.price333.layer.cornerRadius = 2.0
                    self.price333.attributedText = attr
                    return (self.price333.text!,self.price333.tag,index:index)
                default:
                    break
                }
            }
        }
        return ("",0,0)
    }
    
    func removeSelection(index:Int,price:String)
    {
        switch index {
        case 0:
            let price = self.price1.text
            let attr = NSMutableAttributedString(string: price!)
            attr.addAttribute(NSAttributedString.Key.font, value: UIFont(name: kAppFontBold, size: 12.0)!, range: NSMakeRange(0, price!.count))
            attr.addAttribute(NSAttributedString.Key.foregroundColor, value: hexStringToUIColor(hex: kdefalutcolor), range:NSMakeRange(0, price!.count))
            self.price1.layer.borderColor = hexStringToUIColor(hex: kdefalutcolor).cgColor
            self.price1.layer.borderWidth = 0.0
            self.price1.layer.cornerRadius = 2.0
            self.price1.attributedText = attr
            return
        case 1:
            let price = self.price2.text
            let attr = NSMutableAttributedString(string: price!)
            attr.addAttribute(NSAttributedString.Key.font, value: UIFont(name: kAppFontBold, size: 12.0)!, range: NSMakeRange(0, price!.count))
            attr.addAttribute(NSAttributedString.Key.foregroundColor, value: hexStringToUIColor(hex: kdefalutcolor), range:NSMakeRange(0, price!.count))
            self.price2.layer.borderColor = hexStringToUIColor(hex: kdefalutcolor).cgColor
            self.price2.layer.borderWidth = 0.0
            self.price2.layer.cornerRadius = 2.0
            self.price2.attributedText = attr
            return
        case 2:
            let price = self.price3.text
            let attr = NSMutableAttributedString(string: price!)
            attr.addAttribute(NSAttributedString.Key.font, value: UIFont(name: kAppFontBold, size: 12.0)!, range: NSMakeRange(0, price!.count))
            attr.addAttribute(NSAttributedString.Key.foregroundColor, value: hexStringToUIColor(hex: kdefalutcolor), range:NSMakeRange(0, price!.count))
            self.price3.layer.borderColor = hexStringToUIColor(hex: kdefalutcolor).cgColor
            self.price3.layer.borderWidth = 0
            self.price3.layer.cornerRadius = 2.0
            self.price3.attributedText = attr
            return
        default:
            break
        }

    }
    func removeSelection1(index:Int,price:String)
      {
          switch index {
          case 0:
              let price = self.price11.text
              let attr = NSMutableAttributedString(string: price!)
              attr.addAttribute(NSAttributedString.Key.font, value: UIFont(name: kAppFontBold, size: 12.0)!, range: NSMakeRange(0, price!.count))
              attr.addAttribute(NSAttributedString.Key.foregroundColor, value: hexStringToUIColor(hex: kdefalutcolor), range:NSMakeRange(0, price!.count))
              self.price11.layer.borderColor = hexStringToUIColor(hex: kdefalutcolor).cgColor
              self.price11.layer.borderWidth = 0.0
              self.price11.layer.cornerRadius = 2.0
              self.price11.attributedText = attr
              return
          case 1:
              let price = self.price22.text
              let attr = NSMutableAttributedString(string: price!)
              attr.addAttribute(NSAttributedString.Key.font, value: UIFont(name: kAppFontBold, size: 12.0)!, range: NSMakeRange(0, price!.count))
              attr.addAttribute(NSAttributedString.Key.foregroundColor, value: hexStringToUIColor(hex: kdefalutcolor), range:NSMakeRange(0, price!.count))
              self.price22.layer.borderColor = hexStringToUIColor(hex: kdefalutcolor).cgColor
              self.price22.layer.borderWidth = 0.0
              self.price22.layer.cornerRadius = 2.0
              self.price22.attributedText = attr
              return
          case 2:
              let price = self.price33.text
              let attr = NSMutableAttributedString(string: price!)
              attr.addAttribute(NSAttributedString.Key.font, value: UIFont(name: kAppFontBold, size: 12.0)!, range: NSMakeRange(0, price!.count))
              attr.addAttribute(NSAttributedString.Key.foregroundColor, value: hexStringToUIColor(hex: kdefalutcolor), range:NSMakeRange(0, price!.count))
              self.price33.layer.borderColor = hexStringToUIColor(hex: kdefalutcolor).cgColor
              self.price33.layer.borderWidth = 0
              self.price33.layer.cornerRadius = 2.0
              self.price33.attributedText = attr
              return
          default:
              break
          }

      }
    func removeSelection2(index:Int,price:String)
         {
             switch index {
             case 0:
                 let price = self.price111.text
                 let attr = NSMutableAttributedString(string: price!)
                 attr.addAttribute(NSAttributedString.Key.font, value: UIFont(name: kAppFontBold, size: 12.0)!, range: NSMakeRange(0, price!.count))
                 attr.addAttribute(NSAttributedString.Key.foregroundColor, value: hexStringToUIColor(hex: kdefalutcolor), range:NSMakeRange(0, price!.count))
                 self.price111.layer.borderColor = hexStringToUIColor(hex: kdefalutcolor).cgColor
                 self.price111.layer.borderWidth = 0.0
                 self.price111.layer.cornerRadius = 2.0
                 self.price111.attributedText = attr
                 return
             case 1:
                 let price = self.price222.text
                 let attr = NSMutableAttributedString(string: price!)
                 attr.addAttribute(NSAttributedString.Key.font, value: UIFont(name: kAppFontBold, size: 12.0)!, range: NSMakeRange(0, price!.count))
                 attr.addAttribute(NSAttributedString.Key.foregroundColor, value: hexStringToUIColor(hex: kdefalutcolor), range:NSMakeRange(0, price!.count))
                 self.price222.layer.borderColor = hexStringToUIColor(hex: kdefalutcolor).cgColor
                 self.price222.layer.borderWidth = 0.0
                 self.price222.layer.cornerRadius = 2.0
                 self.price222.attributedText = attr
                 return
             case 2:
                 let price = self.price333.text
                 let attr = NSMutableAttributedString(string: price!)
                 attr.addAttribute(NSAttributedString.Key.font, value: UIFont(name: kAppFontBold, size: 12.0)!, range: NSMakeRange(0, price!.count))
                 attr.addAttribute(NSAttributedString.Key.foregroundColor, value: hexStringToUIColor(hex: kdefalutcolor), range:NSMakeRange(0, price!.count))
                 self.price333.layer.borderColor = hexStringToUIColor(hex: kdefalutcolor).cgColor
                 self.price333.layer.borderWidth = 0
                 self.price333.layer.cornerRadius = 2.0
                 self.price333.attributedText = attr
                 return
             default:
                 break
             }
         }

      
      //MARK:- Proceed Action after chosing of prodcuts
      @IBAction func proceedAction(_ sender: Any) {
        self.insertPaymnetDetails()
    }
    
     func showAlert()
    {
        let alertController = UIAlertController(title: "Alert", message: "Please select the Package for CheckOut", preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
        })
        alertController.addAction(alertAction)
        present(alertController, animated: true, completion: nil)
    }
    func removeaddOn(row:Int)
    {
        var clearProd = self.addonprods
        var i = 0
        if addonprods.count > 0
        {
          for dict in addonprods
          {
            for (key, _) in dict
            {
                let keyvalue = key as! String
                if keyvalue == "AddOn\(row)"
                {
                    clearProd.remove(at: i)
                }
            }
             i = i + 1
          }
        }
        self.addonprods = clearProd
    }
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        self.navigationController?.popToRootViewController(animated: true)
       // self.navigationController?.popViewController(animated: true)
    }
}
extension ChooseProductViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return addOnCat.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kCell, for: indexPath) as! AddOnCell
        cell.layer.cornerRadius = 3.0
        cell.layer.borderWidth = 2.0
        cell.layer.borderColor = UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1.0).cgColor
        cell.title.backgroundColor = hexStringToUIColor(hex: "#001442")
        let dict = addOnCat[indexPath.row] as! NSDictionary
        
          cell.title.text = iskeyexist(dict: dict, key: kproductname)
          cell.title.layer.cornerRadius = 3.0
          cell.addon.layer.cornerRadius = 5.0
          cell.addon.text = "ADD TO PLAN"
          if pannetype == "GBP"
          {
              cell.price.text = "£\(iskeyexist(dict: dict, key: "price7"))"
          }
          if pannetype == "EURO"
          {
              cell.price.text = "€\(iskeyexist(dict: dict, key: "price4"))"
          }
          if pannetype == "USD"
          {
              cell.price.text = "$\(iskeyexist(dict: dict, key: "price1"))"
          }
          return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
     return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height:CGFloat = 100
        let width:CGFloat = 100
      return CGSize(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! AddOnCell
        cell.addon.text = "ADDED"
        let price = cell.price.text!
        let cost = price.dropFirst()
        let addOndict = addOnCat[indexPath.row] as! NSDictionary
        self.addOntotal = self.addOntotal + Int(cost)!
        let dict = NSMutableDictionary()
        dict.setValue(cell.title.text, forKey: "AddOn\(indexPath.row)")
        dict.setValue(cost, forKey: "Price\(indexPath.row)")
        dict.setValue(addOndict.value(forKey: "regproducts_id"), forKey: "regproducts_id")
        dict.setValue("Addon", forKey: "title")
        self.addonprods.append(dict)
        self.updateTotalPrice(price: price, type: "add")
     }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! AddOnCell
        cell.addon.text = "ADD TO PLAN"
        let price = cell.price.text!
        let cost = price.dropFirst()
        self.addOntotal = self.addOntotal - Int(cost)!
        removeaddOn(row:indexPath.row)
        self.updateTotalPrice(price: price, type: "minus")
    }
    
    //MARK:- Insert Payment Details
    
    func insertPaymnetDetails()
    {

        let prod = ChoosenProducts()
        let packagearr = NSMutableArray()
        if self.prods.count > 0
        {
            prod.conf_id = iskeyexist(dict: confDict, key: kid)
            prod.conf_name = (iskeyexist(dict: confDict, key: ktitle)).htmlToString
            prod.currency = self.pannetype
            prod.total_Price = self.totalProdPrice
            prod.packages = self.prods
            prod.addons = self.addonprods

            for dict in prod.packages
            {
                let package = ["product_id":dict.value(forKey: "regproducts_id"),"product_type":dict.value(forKey: "title")]
                packagearr.add(package)
            }
            if addonprods.count>0
            {
                for dict in prod.addons
                {
                    let package = ["product_id":dict.value(forKey: "regproducts_id"),"product_type":dict.value(forKey: "title")]
                    packagearr.add(package)
                }
            }
            let fname = userDefaults.value(forKey: "fname")
            let email = userDefaults.value(forKey: "email")!
            let country = userDefaults.value(forKey: "country")!
            let phone = userDefaults.value(forKey: "phone")!
            let app_user_id  = userDefaults.value(forKey: kappuserId)!
            choosenProdDict = ["conf_id":prod.conf_id,"conf_title":prod.conf_name,"name":fname!,"email":email,"phone":phone,"country":country,"amount":"\(prod.total_Price)","gateway":"stripe","payment_page":"ios","currency":prod.currency,"products":packagearr,"app_user_id":app_user_id]
            
            
        self.view.addActivity()
        let obj = NetworkManager(utility: networkUtility!)
        
        let confID = choosenProdDict.value(forKey: "conf_id") as! String
        let confTitle = choosenProdDict.value(forKey: "conf_title") as! String
        let confName = choosenProdDict.value(forKey: "name") as! String
        let confEmail = choosenProdDict.value(forKey: "email") as! String
        let confMobileNum = choosenProdDict.value(forKey: "phone") as! String
        let confCountry = choosenProdDict.value(forKey: "country") as! String
        let confAmnt = choosenProdDict.value(forKey: "amount") as! String
        let confGateway = choosenProdDict.value(forKey: "gateway") as! String
        let confPaymentPage = choosenProdDict.value(forKey: "payment_page") as! String
        let appuserid = choosenProdDict.value(forKey: "app_user_id") as! String
        let confCurrency = choosenProdDict.value(forKey: "currency") as! String
        let confProducts = choosenProdDict.value(forKey: "products") as! NSArray

            let params = ["conf_id":confID,"conf_title":confTitle,"name":confName,"email":confEmail,"phone":confMobileNum,"country":confCountry,"amount":confAmnt,"gateway":confGateway,"payment_page":confPaymentPage,"currency":confCurrency,"products":confProducts,"app_user_id":appuserid] as [String : Any]
          
        
        obj.postRequestApiWith(api: kInsertPaymentRegistrattion, params: params){ (response) in
        if response != nil
        {
            if response is NSDictionary
            {
                let dict = response as! NSDictionary
                let status = iskeyexist(dict: dict, key: kstatus)
                if status == "1"
                {
                    if dict.value(forKey: "registration_details") != nil
                    {
                        self.registrationDetailsArray = dict.value(forKey: "registration_details") as? NSArray ?? []
                        let dataDict = self.registrationDetailsArray.object(at: 0) as! NSDictionary
                        
                        let storyBoard = UIStoryboard(name: kMain, bundle: nil)
                        let vc = storyBoard.instantiateViewController(withIdentifier: "FinalCheckOutVC") as! FinalCheckOutViewController
                        vc.registrationID = dataDict.value(forKey: "reg_id") as! String
                        vc.paymentInsertionDict = dataDict
                        vc.productSelectDict = self.prods
                        vc.selectedaAddOnProds = self.addonprods
                        vc.currency = self.pannetype
                        vc.conftitle = confTitle
                        self.navigationController?.pushViewController(vc, animated: true)

                    }
                }
            }
        }
        else
        {
            DispatchQueue.main.async {
                self.view.removeActivity()
            }
        }

        }
    }
    else
    {
        print("Please select the Package for CheckOut")
        self.showAlert()
    }
  }

}
