//
//  RegistrationViewController.swift
//  ConferenceSeries
//
//  Created by manikumar on 11/03/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import UIKit


class RegistrationViewController: UIViewController,sentcountry {
    
    @IBOutlet weak var fname:UITextField!
    @IBOutlet weak var phoneNumber:UITextField!
    @IBOutlet weak var email:UITextField!
    @IBOutlet weak var country:CustomTextField!
    @IBOutlet weak var countrycode:UILabel!
    @IBOutlet weak var submitBtn: UIButton!
    var isSignedfromGoogle = false
    var isSignedfromApple = false
    var profileinfo = [ProfileInfo]()
    var confDict = NSDictionary()
    var isfromsignin = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fname.setBottomBorder(color: UIColor.darkGray)
        phoneNumber.setBottomBorder(color: UIColor.darkGray)
        email.setBottomBorder(color: UIColor.darkGray)
        email.delegate = self
        phoneNumber.delegate = self
        country.setBottomBorder(color: UIColor.darkGray)
        country.setPaddingWithIcon(image: UIImage(named: "sort-down")!)
        submitBtn.layer.cornerRadius = 2.0
        
        country.addTarget(self, action: #selector(myTargetFunction), for: UIControl.Event.touchDown)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
                   self,
                   selector: #selector(keyboardWillHide),
                   name: UIResponder.keyboardWillHideNotification,
                   object: nil
               )
        if isSignedfromGoogle
        {
            email.isUserInteractionEnabled = false
            fname.text = profileinfo.first?.fullname
            email.text = profileinfo.first?.email
        }
        

    }
    
    
    
    
    

    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    @IBAction func submit(_ sender: UIButton)
    {
        self.view.endEditing(true)
        if (fname.text?.isEmpty)! || (email.text?.isEmpty)! || (country.text?.isEmpty)! || (phoneNumber.text?.isEmpty)!
        {
            self.showAlertWith(title: "Failure", message: "All fields are mandatory")
        }
        else
        {
            let validemail = (isValidEmailAddress(emailAddressString:(email.text!)))
            let validcontact = (isvalidcontact(value: ("\(countrycode.text!)" + phoneNumber.text!)))
            if validemail == false
            {
                // Show Alert message
                self.showAlertWith(title: "Failure", message: "Please enter valid EmailID")
            }
            if validcontact == false
            {
                // Show Alert message
                self.showAlertWith(title: "Failure", message: "Please enter valid Contact")
            }
            if validemail && validcontact
            {
                userDefaults.setValue(fname.text, forKey: "fname")
                userDefaults.setValue(email.text, forKey: "email")
                userDefaults.setValue(country.text, forKey: "country")
                userDefaults.setValue("\(countrycode.text!)" + "\(phoneNumber.text!)", forKey: "phone")
               
               getRegistedProds()
            }
        }
    }
    func getRegistedProds()
    {
        self.view.addActivity()
        let obj = NetworkManager(utility: networkUtility!)
        let conf_id = self.confDict.value(forKey: kid)!
        let params = [kconf_id:conf_id]
        
        obj.postRequestApiWith(api: kProductDetails, params: params){ (response) in
        if response != nil
        {
            let dict = response as! NSDictionary
            if dict.count > 0
            {
                if dict.value(forKey: kregisteredPrd) != nil
                {
                    let arr = dict.value(forKey: kregisteredPrd) as! NSArray
                    if arr.count > 0
                    {
                        let storyBoard = UIStoryboard(name: kMain, bundle: nil)
                        let vc = storyBoard.instantiateViewController(withIdentifier: "ProductVC") as! ChooseProductViewController
                        vc.confDict = self.confDict
                        vc.Productarr = arr
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    else
                    {
                        self.showAlertWith(title:"Failure", message: "Registered Products are not availble for this Conference")
                    }
                    
                }
                else
                {
                    self.showAlertWith(title:"Failure", message: "Registered Products are not availble for this Conference")
                }
                 
            }
            else
            {
                self.showAlertWith(title:"Failure", message: "Registered Products are not availble for this Conference")
            }
            DispatchQueue.main.async {
                self.view.removeActivity()
            }
    
        }
        else
        {
            DispatchQueue.main.async {
                self.view.removeActivity()
            }
        }
    }
        
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardSize.height
            if self.view.frame.origin.y == 0
            {
                self.view.frame.origin.y = -50
            }
        }
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0
            {
                self.view.frame.origin.y = 0
            }
       }
    
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = kEmailFormat
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    func isvalidcontact(value: String) -> Bool {
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", kContactFormat)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    @objc func myTargetFunction(textField: UITextField) {
               let storyBoard = UIStoryboard(name: "Main", bundle: nil)
               let viewTobeLoad = storyBoard.instantiateViewController(withIdentifier: "Country") as! SelectCountryVC
               viewTobeLoad.delegate = self
               viewTobeLoad.iscountry = true
               self.navigationController?.pushViewController(viewTobeLoad, animated: true)
           }
    @IBAction func selectContry(_ sender: UITextField) {
        
        
    }
    func sendcountryname(contryname: String,dialcode: String) {
        DispatchQueue.main.async {
             self.country.text = contryname
             self.countrycode.text = dialcode
             self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func close(_ sender: UIButton)
    {
        if isfromsignin
        {
           if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                sd.gotoDashboard()
            }
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    func showAlertWith(title:String,message:String)
    {
        let alertController = UIAlertController(title: title, message:message , preferredStyle: .alert)
        //to change font of title and message.
        let titleFont = [NSAttributedString.Key.font: UIFont(name: kAppFontBold, size: 14.0)!]
        let messageFont = [NSAttributedString.Key.font: UIFont(name: kAppFont, size: 12.0)!]
        
        let titleAttrString = NSMutableAttributedString(string: title, attributes: titleFont)
        let messageAttrString = NSMutableAttributedString(string: message, attributes: messageFont)
        alertController.setValue(titleAttrString, forKey: "attributedTitle")
        alertController.setValue(messageAttrString, forKey: "attributedMessage")
        
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
            if message == "Registered Successfully"
            {
                
            }
        })
//        let titlealertaction = NSMutableAttributedString(string: "OK", attributes: titleFont)
//        alertController.setValue(titlealertaction, forKey: "attributedTitle")
        alertController.addAction(alertAction)
        present(alertController, animated: true, completion: nil)
    }
    
}



extension RegistrationViewController: UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
           if (phoneNumber.text?.count)! >= 14
           {
               let  char = string.cString(using: String.Encoding.utf8)!
               let isBackSpace = strcmp(char, "\\b")
               
               if (isBackSpace == -92) {
                   return true
               }
               
               return false
           }
          
           if textField.tag == 20
           {
            let valid = isValidEmailAddress(emailAddressString: textField.text!)
             if valid == true
             {
               email.setBottomBorder(color: UIColor.darkGray)
             }
             else
             {
               email.setBottomBorder(color: UIColor(red: 252/255, green: 78/255, blue: 8/255, alpha: 1.0))
             }
           }
           return true
       }
}

class CustomTextField:UITextField
{
    var imageIconView = UIImageView()
    var paddingview = UIView()
    let padding = 0
    let size = 10
    override func awakeFromNib()
    {
        super.awakeFromNib()
        paddingview = UIView(frame: CGRect(x: -15, y: 0, width: size+padding, height: size))
        imageIconView = UIImageView(frame: CGRect(x: 0, y: 0, width: size, height: size))
        paddingview.addSubview(imageIconView)
        self.rightViewMode = .always
        self.rightView = paddingview
    }
    
    func setPaddingWithIcon(image:UIImage)
    {
        imageIconView.image = image
    }
    
    deinit {
        
    }
}

private var kAssociationKeyMaxLength: Int = 0
@IBDesignable
class setmaxlenght:UITextField
{
    var imageIconView = UIImageView()
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        if (UI_IDIOM == .pad)
        {
           imageIconView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        }
        else
        {
            imageIconView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        }
        
        imageIconView.contentMode = .scaleAspectFit
        self.leftViewMode = .always
        self.leftView = imageIconView
    }
    
    func setPaddingWithIcon(image:UIImage)
    {
        imageIconView.image = image
    }
    
    deinit {
        
    }

    @IBInspectable var maxLength: Int {
        get {
            if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLength) as? Int {
                return length
            } else {
                return Int.max
            }
        }
        set {
            objc_setAssociatedObject(self, &kAssociationKeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
            addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
        }
    }
    
    @objc func checkMaxLength(textField: UITextField) {
        guard let prospectiveText = self.text,
            prospectiveText.count > maxLength
            else {
                return
        }
        
        let selection = selectedTextRange
        
        let indexEndOfText = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
        let substring = prospectiveText[..<indexEndOfText]
        text = String(substring)
        
        selectedTextRange = selection
    }
    
    let padding = UIEdgeInsets(top: 0, left:30, bottom: 0, right: 5)
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
