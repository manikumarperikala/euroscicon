//
//  SigninViewController.swift
//  ConferenceSeries
//
//  Created by Srinu Babu Gedela on 15/05/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import UIKit

class SigninViewController: UIViewController {
    
    @IBOutlet weak var username:UITextField!
    @IBOutlet weak var password:UITextField!
    
    var confDict = NSDictionary()
    var naviKey = String()

    override func viewDidLoad() {
        super.viewDidLoad()

        username.setBottomBorder(color: UIColor.darkGray)
        password.setBottomBorder(color: UIColor.darkGray)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    @IBAction func tapped(_ sender:UITapGestureRecognizer)
    {
        self.view.endEditing(true)
    }
    
    @IBAction func signin(_ sender:UIButton)
    {
        if ((username.text?.isEmpty)! || (password.text?.isEmpty)!)
        {
            // Alert Please enter Credentails
            self.showAlertWith(title: "Alert", message: "Please Enter credentails")
        }
        else
        {
           // Signin Service
            
            self.view.addActivity()
            let obj = NetworkManager(utility: networkUtility!)

            let params = ["email":username.text!,"password":self.password.text!]
               obj.postRequestApiWith(api: kappLogin, params: params){ (response) in
                     if response != nil
                     {
                         let dict = response as! NSDictionary
                         if dict.count > 0
                         {
                           let status = iskeyexist(dict: dict, key: "status")
                           if status == "true" || status == "1"
                           {
                            let appuser = dict.value(forKey: "app_user") as! NSArray
                            if appuser.count > 0
                            {
                                let innerarr = appuser.firstObject as! NSDictionary
                                let appuserid = iskeyexist(dict: innerarr, key: "app_user_id")
                                userDefaults.set(appuserid, forKey: kappuserId)
                                self.loggedin()
                            }
                            
                           }
                           else
                           {
                               self.showAlertWith(title: "Failure", message: "Invalid Credentails")
                           }
                              
                         }
                         else
                         {
                             self.showAlertWith(title:"Failure", message: "Login Failed")
                         }
                         DispatchQueue.main.async {
                             self.view.removeActivity()
                         }
                 
                     }
                     else
                     {
                         DispatchQueue.main.async {
                             self.view.removeActivity()
                         }
                     }
               
           }
           
        }
        
    }
    func loggedin()
    {
        userDefaults.set("true", forKey: kLoggedIn)
           switch naviKey {
           case "Reg":
              let storyBoard = UIStoryboard(name: kMain, bundle: nil)
              let vc = storyBoard.instantiateViewController(withIdentifier: "Registration") as! RegistrationViewController
              vc.confDict = self.confDict
              vc.isfromsignin = true
              self.navigationController?.pushViewController(vc, animated: true)
               break
           case "Abs":
               let storyBoard = UIStoryboard(name: kMain, bundle: nil)
               let vc = storyBoard.instantiateViewController(withIdentifier: "AbstractVC") as! AbstractViewController
               vc.isfromsignin = true
               self.navigationController?.pushViewController(vc, animated: true)
               break
           case "brouchure":
                let storyBoard = UIStoryboard(name: kMain, bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "brouchure") as! BroucherDownloadVC
                vc.isfromsignin = true
                self.navigationController?.pushViewController(vc, animated: true)
            break
           case "feedback":
               let storyBoard = UIStoryboard(name: kMain, bundle: nil)
               let vc = storyBoard.instantiateViewController(withIdentifier: "feedback") as! feedbackVC
               vc.isfromsignin = true
               self.navigationController?.pushViewController(vc, animated: true)
            break
            case "presentation":
                let storyBoard = UIStoryboard(name: kMain, bundle: nil)
                           let vc = storyBoard.instantiateViewController(withIdentifier: "PresentationVC") as! MyPresentationViewController
                           self.navigationController?.pushViewController(vc, animated: true)
           break
           default:
               break
           }
        DispatchQueue.main.async
        {
                self.view.removeActivity()
        }
    }
    @IBAction func signup(_ sender:UIButton)
    {
        let storyBoard = UIStoryboard(name: kMain, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "signup") as! SignUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }

     func showAlertWith(title:String,message:String)
        {
            let alertController = UIAlertController(title: title, message:message , preferredStyle: .alert)
            //to change font of title and message.
            let titleFont = [NSAttributedString.Key.font: UIFont(name: kAppFontBold, size: 14.0)!]
            let messageFont = [NSAttributedString.Key.font: UIFont(name: kAppFont, size: 12.0)!]
            
            let titleAttrString = NSMutableAttributedString(string: title, attributes: titleFont)
            let messageAttrString = NSMutableAttributedString(string: message, attributes: messageFont)
            alertController.setValue(titleAttrString, forKey: "attributedTitle")
            alertController.setValue(messageAttrString, forKey: "attributedMessage")
            
            let alertAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
               
            })
    //        let titlealertaction = NSMutableAttributedString(string: "OK", attributes: titleFont)
    //        alertController.setValue(titlealertaction, forKey: "attributedTitle")
            alertController.addAction(alertAction)
            present(alertController, animated: true, completion: nil)
        }

}
