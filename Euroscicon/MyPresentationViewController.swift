//
//  MyPresentationViewController.swift
//  ConferenceSeries
//
//  Created by Mallesh Kurva on 09/08/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import UIKit
import MobileCoreServices

var picker = UIImagePickerController()
var videoURL: URL?
var pickedVideoURL = NSString()


class MyPresentationViewController: UIViewController, UIImagePickerControllerDelegate , UINavigationControllerDelegate, sendDetails , UITextFieldDelegate {
    
    var countrycode = String()

    @IBOutlet weak var addressTF: UITextField!
    @IBOutlet weak var phoneNumTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var nameTF: UITextField!
    
    @IBAction func backBtnTapped(_ sender: Any) {
    }
    
    @IBAction func selectTitleTapped(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let viewTobeLoad = storyBoard.instantiateViewController(withIdentifier: "Country") as! SelectCountryVC
        viewTobeLoad.delegate2 = self
        viewTobeLoad.iscountry = false
        viewTobeLoad.isfromabstract = true
        viewTobeLoad.fields = titlearr
        viewTobeLoad.type = "title"
        self.navigationController?.pushViewController(viewTobeLoad, animated: true)
        
    }
    @IBAction func selectCountryTapped(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let viewTobeLoad = storyBoard.instantiateViewController(withIdentifier: "Country") as! SelectCountryVC
        viewTobeLoad.delegate2 = self
        viewTobeLoad.iscountry = true
        viewTobeLoad.isfromabstract = true
        viewTobeLoad.type = "country"
        self.navigationController?.pushViewController(viewTobeLoad, animated: true)

    }
    
    @IBOutlet weak var selectTitleBtn: UIButton!
    @IBOutlet weak var selectCntryBtn: UIButton!
    
    
    @IBAction func audioBtnTapped(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: kMain, bundle: nil)
                   let vc = storyBoard.instantiateViewController(withIdentifier: "AudioVC") as! AudioViewController
                   self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func videoBtnTap(_ sender: Any) {
        self.openCamera()
    }
    
    @IBAction func videoGalleryTap(_ sender: Any) {
        
        self.openVideoGallery()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.view.backgroundColor = UIColor.orange
        
        self.selectTitleBtn.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: -(self.selectTitleBtn.imageView?.frame.size.width)!+(15), bottom: 0, right: (self.selectTitleBtn.imageView?.frame.size.width)!)
        self.selectTitleBtn.imageEdgeInsets = UIEdgeInsets.init(top: 15, left: self.selectTitleBtn.frame.size.width - 10, bottom: 15, right: 2);
              
        self.selectCntryBtn.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: -(self.selectCntryBtn.imageView?.frame.size.width)!+(15), bottom: 0, right: (self.selectCntryBtn.imageView?.frame.size.width)!)
        self.selectCntryBtn.imageEdgeInsets = UIEdgeInsets.init(top: 15, left: self.selectCntryBtn.frame.size.width - 10, bottom: 15, right: 2);
        
        nameTF.delegate = self;
        emailTF.delegate = self;
        phoneNumTF.delegate = self;
        addressTF.delegate = self;

//        NotificationCenter.default.addObserver(
//                   self,
//                   selector: #selector(keyboardWillShow),
//                   name: UIResponder.keyboardWillShowNotification,
//                   object: nil
//               )
//               NotificationCenter.default.addObserver(
//                          self,
//                          selector: #selector(keyboardWillHide),
//                          name: UIResponder.keyboardWillHideNotification,
//                          object: nil
//                      )

        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
     
        // Pass the selected object to the new view controller.
    }
    */
    
    func openVideoGallery() {
        
        picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .savedPhotosAlbum
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .savedPhotosAlbum)!
        picker.mediaTypes = ["public.movie"]
        picker.allowsEditing = false
        present(picker, animated: true, completion: nil)
    }

    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
//        println("captureVideoPressed and camera available.")

        var imagePicker = UIImagePickerController()

        imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.mediaTypes = kUTTypeMovie as! [String]
        imagePicker.allowsEditing = false

        imagePicker.showsCameraControls = true

            self.present(imagePicker, animated: true, completion: nil)
      } else {
//        println("Camera not available.")
      }
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        videoURL = info[UIImagePickerController.InfoKey.mediaURL] as! NSURL? as URL?
        let pathString = videoURL?.relativePath
        pickedVideoURL = (videoURL?.relativePath ?? "") as NSString
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func submitBtnTapped(_ sender: Any) {
    
          self.view.endEditing(true)
        
        print(selectTitleBtn.currentTitle as Any)
        print(selectCntryBtn.currentTitle as Any)
        print(nameTF.text as Any)
        print(emailTF.text as Any)
        print(phoneNumTF.text as Any)
        print(addressTF.text as Any)
        
        
        if (selectTitleBtn.currentTitle == "Select Title") || (nameTF.text?.isEmpty)! ||  selectCntryBtn.currentTitle == "Select Country" || (emailTF.text?.isEmpty)! || (phoneNumTF.text?.isEmpty)! || (addressTF.text?.isEmpty)!
          {
              self.showAlertWith(title: "Alert", message: "All fields are Mandatory")
          }
          else
          {
            
            let validemail = (isValidEmailAddress(emailAddressString:(emailTF.text!)))

            if selectTitleBtn.currentTitle == "Select Title"
            {
                self.showAlertWith(title:"Alert" , message: "Please Select Title")
            }
            if nameTF.text == ""
            {
                self.showAlertWith(title:"Alert" , message: "Please enter the name")
            }
            if selectCntryBtn.currentTitle == "Select Country"
            {
                self.showAlertWith(title:"Alert" , message: "Please Select Title")
            }
            if validemail == false
            {
                // Show Alert message
                self.showAlertWith(title: "Alert", message: "Please enter valid EmailID")
            }
            if phoneNumTF.text == ""
            {
                self.showAlertWith(title:"Alert" , message: "Please enter phone number")
            }
            if addressTF.text == ""
            {
                self.showAlertWith(title:"Alert" , message: "Please select address")
            }
            
            guard let audioPath  = userDefaults.url(forKey: "audioFile") as NSURL?  else {
                
                var FileURLPath = NSURL()

                FileURLPath =  URL(fileURLWithPath: pickedVideoURL as String) as NSURL
                
                            let dateformatter = DateFormatter()
                            dateformatter.dateFormat = kUTCformat
                            let currentdate = dateformatter.string(from:Date())
                            
                            let id = userDefaults.value(forKey: "conf_id") as! String
                            let fname = self.nameTF.text!
                            let email = self.emailTF.text!
                            let country = self.selectCntryBtn.currentTitle!
                            let phone = self.phoneNumTF.text!
                            let address = self.addressTF.text!
                            
                //            let urlPath = userDefaults.value(forKey: "audioFile") as! NSString
                            
                            
                                    let appuserid = userDefaults.value(forKey: kappuserId)!
                                    let params = ["conf_id":id,"title":self.selectTitleBtn.currentTitle!,"name":"\(fname)","country":"\(country)","email":"\(email)","phone":"\(phone)","address":address,"date":currentdate,"source":"ios","app_user_id":appuserid] as [String : Any]
                            
                            
                //            self.uploadFile(img: FileURLPath as URL, params: params)
                            
                            self.uploadFile(img: FileURLPath as URL, params: params)

                
               return
             }
            
            print(audioPath)

            var FileURLPath = NSURL()

//            if(audioPath != nil){
                FileURLPath = audioPath

//            }else{
//                FileURLPath =  URL(fileURLWithPath: pickedVideoURL as String) as NSURL

//            }
            
            let dateformatter = DateFormatter()
            dateformatter.dateFormat = kUTCformat
            let currentdate = dateformatter.string(from:Date())
            
            let id = userDefaults.value(forKey: "conf_id") as! String
            let fname = self.nameTF.text!
            let email = self.emailTF.text!
            let country = self.selectCntryBtn.currentTitle!
            let phone = self.phoneNumTF.text!
            let address = self.addressTF.text!
            
//            let urlPath = userDefaults.value(forKey: "audioFile") as! NSString
            
            
                    let appuserid = userDefaults.value(forKey: kappuserId)!
                    let params = ["conf_id":id,"title":self.selectTitleBtn.currentTitle!,"name":"\(fname)","country":"\(country)","email":"\(email)","phone":"\(phone)","address":address,"date":currentdate,"source":"ios","app_user_id":appuserid] as [String : Any]
            
            
//            self.uploadFile(img: FileURLPath as URL, params: params)
            
            self.uploadFile(img: FileURLPath as URL, params: params)

        }
    }
    
    
    func uploadFile(img:URL,params:[String:Any])
       {
           var imageData:NSData
           guard let fileData = try? NSData(contentsOf: img) else {
             return
           }
           imageData = fileData
//           SVProgressHUD.show(withStatus: "Uploading...")
           self.view.isUserInteractionEnabled = false
           let obj = NetworkManager(utility: networkUtility!)
           obj.uploadImageRequest(api: kAudioVideoSubmission, params: params, image: imageData as Data, completion: {
               (response) in
//                   SVProgressHUD.setStatus("Uploading.....")
                   guard let result = response as? NSDictionary

                   else
                   {
//                       SVProgressHUD.dismiss()
                       self.view.isUserInteractionEnabled = true
                       return
                   }
                   if result["status"] as? Bool == true
                   {

                    self.showAlertWith(title: "Success", message: "Uploaded Successfully")
                    UserDefaults.standard.set(nil, forKey: "audioFile")

//                       SVProgressHUD.setStatus("Uploaded Successfully")
                    self.setdefaults()
                   }
                   else
                   {
                       self.showAlertWith(title: "Upload Failed", message: "Unable to upload Image")
                   }
//                   SVProgressHUD.dismiss()
                   self.view.isUserInteractionEnabled = true
           }, onerror: {
               (error) in

//               SVProgressHUD.dismiss()
               self.showAlertWith(title: "Upload Failed", message: "Unable to upload Document")
               self.view.isUserInteractionEnabled = true

           })
       }
    
    func setdefaults()  {
        
        nameTF.text = ""
        emailTF.text = ""
        selectTitleBtn.setTitle("Select Title", for: UIControl.State.normal)
        selectCntryBtn.setTitle("Select Country", for: UIControl.State.normal)
        addressTF.text = ""
        phoneNumTF.text = ""
        
    }
    
    // Delegate method
    func details(titlename: String, type: String, countrycode: String) {

           switch type{
           case "title":
               self.selectTitleBtn.setTitle(titlename, for: .normal)
               break
           case "country":
               self.selectCntryBtn.setTitle(titlename, for: .normal)
//               self.countrycode = countrycode
               break
           
           default:
               self.selectTitleBtn.setTitle("Select Title", for: .normal)
               self.selectCntryBtn.setTitle("Select Country", for: .normal)
               break
           }
           self.navigationController?.popViewController(animated: false)
       }
    
    // Alert controller
    func showAlertWith(title:String,message:String)
        {
            let alertController = UIAlertController(title: title, message:message , preferredStyle: .alert)
            //to change font of title and message.
            let titleFont = [NSAttributedString.Key.font: UIFont(name: kAppFontBold, size: 14.0)!]
            let messageFont = [NSAttributedString.Key.font: UIFont(name: kAppFont, size: 12.0)!]
            
            let titleAttrString = NSMutableAttributedString(string: title, attributes: titleFont)
            let messageAttrString = NSMutableAttributedString(string: message, attributes: messageFont)
            alertController.setValue(titleAttrString, forKey: "attributedTitle")
            alertController.setValue(messageAttrString, forKey: "attributedMessage")
            
            let alertAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
               
            })
    //        let titlealertaction = NSMutableAttributedString(string: "OK", attributes: titleFont)
    //        alertController.setValue(titlealertaction, forKey: "attributedTitle")
            alertController.addAction(alertAction)
            present(alertController, animated: true, completion: nil)
        }
    
    @objc func keyboardWillShow(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
//            let keyboardHeight = keyboardSize.height
//            if self.view.frame.origin.y == 0
//            {
//                self.view.frame.origin.y = -50
//            }
//        }
    }
    @objc func keyboardWillHide(notification: NSNotification) {
//        if self.view.frame.origin.y != 0
//            {
//                self.view.frame.origin.y = 0
//            }
       }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
}

