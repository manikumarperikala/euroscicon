//
//  CheckOutViewController.swift
//  ConferenceSeries
//
//  Created by Mallesh Kurva on 08/04/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import UIKit
import Stripe

class CheckOutViewController: UIViewController,STPPaymentCardTextFieldDelegate {
    let cardParams = STPCardParams()
    
    var registrationID = String()
    var paymentInsertionDict = NSDictionary()
    
    
    @IBOutlet weak var btnBuy: UIButton!
    @IBAction func OnPayBtnTapped(_ sender: UIButton) {
        
        self.actionGetStripeToken()
        
    }
    override func viewDidLoad()  {
        super.viewDidLoad()
        
        print(paymentInsertionDict)
        
        let paymentField = STPPaymentCardTextField(frame: CGRect(x: 10, y: 400, width:self.view.frame.size.width - 20, height: 44))
        paymentField.delegate = self as? STPPaymentCardTextFieldDelegate
        self.view.addSubview(paymentField)

//      paymentField.backgroundColor = UIColor.orange
        
        // Do any additional setup after loading the view.
    }
    
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        print("Card number: \(String(describing: textField.cardParams.number)) Exp Month: \(String(describing: textField.cardParams.expMonth)) Exp Year: \(String(describing: textField.cardParams.expYear)) CVC: \(String(describing: textField.cardParams.cvc))")
    self.btnBuy.isEnabled = textField.isValid

    if btnBuy.isEnabled {
    btnBuy.backgroundColor = UIColor.blue
    cardParams.number = textField.cardParams.number
        cardParams.expMonth = textField.cardParams.expMonth as! UInt
        cardParams.expYear = textField.cardParams.expYear as! UInt
    cardParams.cvc = textField.cardParams.cvc
    }
    }
    
    @IBAction func actionGetStripeToken() {
        STPAPIClient.shared().createToken(withCard: cardParams) { (token, error) in
    if let error = error {
    // show the error to the user
    print(error)
        self.showAlertButtonTapped(strTitle: "Error", strMessage: error.localizedDescription)
    } else if let token = token {
    print(token)
    //Send token to backend for process
    }
    }
    }

    //MARK:- AlerViewController
    func showAlertButtonTapped(strTitle:String, strMessage:String) {
    // create the alert
        let alert = UIAlertController(title: strTitle, message: strMessage, preferredStyle: UIAlertController.Style.alert)
    // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
    // show the alert
        self.present(alert, animated: true, completion: nil)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    @IBAction func donate(sender: AnyObject) {

    }
}
