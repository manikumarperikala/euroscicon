//
//  ActivityView.swift
//  ConferenceSeries
//
//  Created by Manikumar on 22/03/18.
//  Copyright © 2018 Omics. All rights reserved.
//

import UIKit

class ActivityView: UIView {

}

extension UIView{
    
    func addActivity(){
        let activity = Bundle.main.loadNibNamed(kActivityView, owner: self, options: nil)?.first as! ActivityView
        activity.frame = self.bounds
        addSubview(activity)
    }
    
    func removeActivity(){
        for view in self.subviews{
            if view is ActivityView{
                view.removeFromSuperview()
            }
        }
    }
}
