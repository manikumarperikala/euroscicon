//
//  AudioViewController.swift
//  ConferenceSeries
//
//  Created by Mallesh Kurva on 10/08/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import UIKit
import AVFoundation

class AudioViewController: UIViewController ,AVAudioRecorderDelegate, AVAudioPlayerDelegate {
    
    @IBOutlet weak var timerLbl: UILabel!
    var timer: Timer?
    var totalTime = 0
    var fileURL = NSURL()
    
    @IBOutlet var recordButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var uploadBtn: UIButton!
    
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var audioPlayer:AVAudioPlayer!
    
    @IBAction func recordBtnTap(_ sender: Any) {
        
        if audioRecorder == nil {
            
            playButton.backgroundColor = UIColor.lightGray
            uploadBtn.backgroundColor = UIColor.lightGray
            
            playButton.isEnabled = false
            uploadBtn.isEnabled = false
            
            startRecording()
            self.startOtpTimer()
            
            
        } else {
            
            playButton.backgroundColor = UIColor.black
            uploadBtn.backgroundColor = UIColor.black
            
            playButton.isEnabled = true
            uploadBtn.isEnabled = true
            
            finishRecording(success: true)
        }

    }
    
    func startRecording() {
        let audioFilename = getFileURL()
        
        fileURL = audioFilename as NSURL
        
//        let weblink = fileURL as! String
        
        var weblink: String = fileURL.relativeString

        if let URL = URL(string: weblink){
            
            print(URL)

            
            UserDefaults.standard.set(URL, forKey: "audioFile")
        }

        
//        userDefaults.setValue(fileURL, forKey: "audioFile")
        
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        do {
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.record()
            
            recordButton.setTitle("Stop", for: .normal)
            playButton.isEnabled = false
        } catch {
            finishRecording(success: false)
        }
    }
    
    func finishRecording(success: Bool) {
        audioRecorder.stop()
        audioRecorder = nil
        
        if success {
            recordButton.setTitle("Re-record", for: .normal)
            
            self.timer?.invalidate()
            self.timer = nil

            self.timerLbl.text = "00 : 00 : 00"

            
        } else {
            recordButton.setTitle("Record", for: .normal)
            // recording failed :(
        }
        
        playButton.isEnabled = true
        recordButton.isEnabled = true
    }
    
    @IBAction func playBtnTap(_ sender: Any) {
        
        if ((sender as AnyObject).titleLabel?.text == "Play"){
                   recordButton.isEnabled = false
//                   sender.setTitle("Stop", for: .normal)
            playButton.setTitle("Stop", for: .normal)
                   preparePlayer()
                   audioPlayer.play()
            
                  self.startOtpTimer()
            
               } else {
            
                   audioPlayer.stop()
                   playButton.setTitle("Play", for: .normal)
                       
            self.timer?.invalidate()
            self.timer = nil

            self.timerLbl.text = "00 : 00 : 00"
            
//dude - are you der  ???
            
               }

    }
    
    func preparePlayer() {
        var error: NSError?
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: getFileURL() as URL)
        } catch let error1 as NSError {
            error = error1
            audioPlayer = nil
        }
        
        if let err = error {
            print("AVAudioPlayer error: \(err.localizedDescription)")
        } else {
            audioPlayer.delegate = self
            audioPlayer.prepareToPlay()
            audioPlayer.volume = 10.0
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func getFileURL() -> URL {
        let path = getDocumentsDirectory().appendingPathComponent("recording.m4a")
        return path as URL
    }
    
    //MARK: Delegates
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        print("Error while recording audio \(error!.localizedDescription)")
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        recordButton.isEnabled = true
        playButton.setTitle("Play", for: .normal)
        
               audioPlayer.stop()
               playButton.setTitle("Play", for: .normal)
                   
        self.timer?.invalidate()
        self.timer = nil

        self.timerLbl.text = "00 : 00 : 00"

        
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print("Error while playing audio \(error!.localizedDescription)")
    }

    
    @IBAction func uploadBtnTap(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)

        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playButton.backgroundColor = UIColor.lightGray
        uploadBtn.backgroundColor = UIColor.lightGray
        
        playButton.isEnabled = false
        uploadBtn.isEnabled = false
        
        self.setupView()

        // Do any additional setup after loading the view.
    }
    
    func setupView() {
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        self.loadRecordingUI()
                    } else {
                        // failed to record
                    }
                }
            }
        } catch {
            // failed to record
        }
    }

    func loadRecordingUI() {
        recordButton.isEnabled = true
        playButton.isEnabled = false
        recordButton.setTitle("Record", for: .normal)
//        recordButton.addTarget(self, action: #selector(recordAudioButtonTapped), for: .touchUpInside)
        

        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    private func startOtpTimer() {
           self.totalTime = 0
           self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
        

       }

    @objc func updateTimer() {
            print(self.totalTime)
        
//        totalTime += 1     //This will decrement(count down)the seconds.
//        timerLbl.text = "\(totalTime)" //This will update the label.
        
//            if totalTime != 0 {
                totalTime += 1  // increase counter timer
                self.timerLbl.text = self.timeFormatted(self.totalTime) // will show timer

//            } else {
//                if let timer = self.timer {
//                    timer.invalidate()
//                    self.timer = nil
//                }
//            }
        }
    func timeFormatted(_ totalSeconds: Int) -> String {
        
        let hours : Int = totalSeconds / 3600
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%02d : %02d : %02d",hours, minutes, seconds)
    }

    
    
}
