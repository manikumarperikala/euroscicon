//
//  SessionTracksVC.swift
//  ConferenceSeries
//
//  Created by Srinu Babu Gedela on 09/06/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import UIKit

class SessionTracksVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableview:UITableView!
    var trackArray = NSArray()
    var sessions = [Tracks]()
    private var openSection: Int = 0
 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Sessions & Tracks"
        tableview.estimatedRowHeight = 120
        tableview.rowHeight = UITableView.automaticDimension
        tableview.register(UINib(nibName: "TextviewCell", bundle: nil), forCellReuseIdentifier: "idCellTextview")
        tableview.register(UINib(nibName: "SwitchCell", bundle: nil), forCellReuseIdentifier: "idCellNormal")
        tableview.backgroundColor = UIColor.white
         self.tableview.tableFooterView = UIView()
        self.getsessiontracks()
    }
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          self.navigationController?.setNavigationBarHidden(false, animated: true)
      }
    
    func getsessiontracks()
    {
        self.view.addActivity()
        let obj = NetworkManager(utility: networkUtility!)
        let id = userDefaults.value(forKey: "conf_id") as! String
        let params = ["conf_id":id]
        obj.postRequestApiWith(api: ksessiontracks, params: params){ (response) in
          if response != nil
          {
            let dict = response as? NSDictionary ?? [:]
            if dict.count > 0
            {
                let status = iskeyexist(dict: dict, key: kstatus)
                if status == "1"
                {
                    if dict.value(forKey: "tracks") != nil
                    {
                      let tracks = dict.value(forKey: "tracks") as? NSArray ?? []
                        if tracks.count > 0
                        {
                           for eachtrack in tracks
                           {
                              let innertrack = eachtrack as! NSDictionary
                              let track_id = innertrack.value(forKey: "id") as! String
                              let track_name = innertrack.value(forKey: "TrackName") as! String
                              let track_desc = innertrack.value(forKey: "description") as! String
                              let subtracks = innertrack.value(forKey: "sub_tracks") as? NSArray ?? []
                              var sub_trackslist = [String]()
                            sub_trackslist.append("")
                            if subtracks.count > 0
                            {
                                for sub_tracks in subtracks
                                {
                                    let trackname = ((sub_tracks as! NSDictionary).value(forKey: "TrackName") as! String)
                                    sub_trackslist.append(trackname)
                                }
                            }
                            let tracksfinal = Tracks(track_id: track_id, track_name: track_name, track_desc: track_desc, sub_tracks: sub_trackslist, isSelected: false)
                            self.sessions.append(tracksfinal)
                           }
                        }
                      
                    }
                }
                if self.sessions.count > 0
                {
                    DispatchQueue.main.async {
                      self.view.removeActivity()
                      self.tableview.reloadData()
                      
                    }
                }
            }
              
          }
          else
          {
              DispatchQueue.main.async {
                  self.view.removeActivity()
              }
          }
                      
          }
     
    }
    
    //MARK:- TableView Delegation
    func numberOfSections(in tableView: UITableView) -> Int {
        if sessions.count > 0 {
              
            tableview.backgroundView = nil
              return sessions.count
        }
        else
        {
            let messageLabel = UILabel(frame: tableview.frame)
            messageLabel.text = "Sessions & Tracks Not available"
            messageLabel.textAlignment = .center
            tableview.backgroundView = messageLabel
        }
        return 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ((sessions)[section].isSelected)
        {
             if (((sessions)[section]).sub_tracks.count) > 0
             {
                return (((sessions)[section]).sub_tracks.count)
             }
        }
        return 0
     
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var tableviewcell = UITableViewCell()
        if indexPath.row == 0
        {
        let cell = (tableView.dequeueReusableCell(withIdentifier: "idCellTextview", for: indexPath) as? CustomCell)!
        let data = sessions[indexPath.section]
       
        cell.textview.text = (data.track_desc).htmlToString
        if UI_IDIOM == .pad
         {
            cell.textview.font = UIFont(name: kAppFont, size: 18.0)
         }
         else
         {
            cell.textview.font = UIFont(name: kAppFont, size: 13.0)
         }
            
       
        cell.textview.isSelectable = false
        cell.textview.isEditable =  false
        cell.textview.textColor = UIColor.black
        cell.textview.layer.borderColor = UIColor.gray.cgColor
        cell.textview.layer.borderWidth = 1.0
        cell.textview.layer.cornerRadius = 2.0
        cell.textview.backgroundColor = hexStringToUIColor(hex: "#bee1f1")
        cell.textview.textAlignment = .justified
        cell.textview.isScrollEnabled = false
        cell.txtVwWidthConst.constant = self.tableview.frame.size.width
        cell.textview.adjustUITextViewHeight()
        tableviewcell = cell
        }
        else
        {
            let cell = (tableView.dequeueReusableCell(withIdentifier: "idCellNormal", for: indexPath) as? CustomCell)!
            let data = (sessions[indexPath.section]).sub_tracks
            cell.titleLbl.text = "Track\(indexPath.section+1)-\(indexPath.row+1)" + " " + data[indexPath.row]
            if UI_IDIOM == .pad
            {
                cell.titleLbl.font = UIFont(name: kAppFont, size: 18.0)
            }
            else
            {
                cell.titleLbl.font = UIFont(name: kAppFont, size: 13.0)
            }
           
            cell.titleLbl.textColor = UIColor.black
            tableviewcell = cell
        }
        
        tableviewcell.contentView.setNeedsLayout()
        tableviewcell.contentView.layoutIfNeeded()
    
        return tableviewcell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0
        {
              return UITableView.automaticDimension
        }
        return 50
      
    }
    
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
         let headerView = UIView()
    
        headerView.backgroundColor = UIColor.clear
        headerView.layer.borderWidth = 1.0
        headerView.layer.cornerRadius = 2.0
        headerView.layer.borderColor = UIColor.gray.cgColor
        headerView.tag  = section
        let tap = UITapGestureRecognizer(target: self, action:#selector(self.sectionTap(_:)))
        headerView.addGestureRecognizer(tap)
        let headerLabel = UILabel(frame: CGRect(x: 10, y: 10, width:
             tableView.bounds.size.width-50, height: tableView.bounds.size.height))
        if UI_IDIOM == .pad
        {
            headerLabel.font = UIFont(name: kAppFont, size: 20.0)
        }
         else
        {
            headerLabel.font = UIFont(name: kAppFont, size: 15.0)
        }
         headerLabel.numberOfLines = 0
         headerLabel.textColor = UIColor.black
         headerLabel.text = (sessions)[section].track_name
         headerView.backgroundColor = hexStringToUIColor(hex: "#eeeeee")
         headerLabel.sizeToFit()
         headerView.addSubview(headerLabel)
        
        let btn = UIButton(frame: CGRect(x: tableView.bounds.size.width-20
            , y: 15, width: 10, height: 10))
        
        if (sessions)[section].isSelected
        {
            btn.isSelected = true
        }
        btn.setImage(UIImage(named : "plus1"), for: UIControl.State.normal)
        btn.setImage(UIImage(named : "minus1"), for: UIControl.State.selected)
        btn.tag = section
        btn.imageView?.contentMode = .scaleAspectFit
       // btn.addTarget(self, action: #selector(selecttrack), for:   .primaryActionTriggered)
        headerView.addSubview(btn)
    
                 return headerView
             }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }
    @objc func selecttrack(_ sender:UIButton)
       {
           if sender.isSelected == true
           {
               let data = sessions
               ((data[sender.tag])).isSelected = false
               sender.isSelected = false
           }
           
           else
           {
               let data = sessions
               ((data[sender.tag])).isSelected = true
               sender.isSelected = true
           }
           DispatchQueue.main.async {
               self.tableview.reloadSections(NSIndexSet(index: sender.tag) as IndexSet, with: .automatic)
           }
      
       }
    //when the user taps on the section
    @objc func sectionTap(_ sender: UITapGestureRecognizer) {
        
        let newSection = sender.view!.tag
        
        //only do anything if new section is different
           let data = sessions
        let collapsible = data[newSection].isSelected
           if collapsible == true
           {
              data[newSection].isSelected = false
           }
           else
           {
              data[newSection].isSelected = true
           }
           var sectionArray: [Int] = [openSection]
           sectionArray.append(newSection)
           openSection = newSection
          
           //list of original open section and the new section that we need to expand
           let indices: IndexSet = IndexSet(sectionArray)
           //refresh those sections
           tableview.reloadSections(indices, with: .automatic)
    }
    
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension UITextView {
    func adjustUITextViewHeight() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.sizeToFit()
        self.isScrollEnabled = false
    }
}
