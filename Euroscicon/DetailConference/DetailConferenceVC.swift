//
//  DetailConferenceVC.swift
//  ConferenceSeries
//
//  Created by manikumar on 09/03/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import UIKit
import AlamofireImage




class DetailConferenceVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
 
    @IBOutlet weak var collectionView:UICollectionView!
    
   
    @IBOutlet weak var conferencetitle: UILabel!
    
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var conftypeImg: UIImageView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var PageControl: UIPageControl!
    @IBOutlet weak var registration: UIButton!
    @IBOutlet weak var hotel: UIButton!
    
    @IBOutlet weak var hotelwidth: NSLayoutConstraint!
    @IBOutlet weak var hotelheight: NSLayoutConstraint!
    @IBOutlet weak var travelwidth: NSLayoutConstraint!
    @IBOutlet weak var travelheight: NSLayoutConstraint!
    @IBOutlet weak var sightwidth: NSLayoutConstraint!
    @IBOutlet weak var sightheight: NSLayoutConstraint!
    @IBOutlet weak var diningwidth: NSLayoutConstraint!
    @IBOutlet weak var diningheight: NSLayoutConstraint!
    @IBOutlet weak var backBtn:UIBarButtonItem!
    @IBOutlet weak var tial1: UIView!
    @IBOutlet weak var tail2:UIView!
    @IBOutlet weak var tial3:UIView!
    @IBOutlet weak var schdule:UIButton!
    @IBOutlet weak var nwk:UIButton!
    
    @IBOutlet weak var othersViewhgt: NSLayoutConstraint!
    var sliderImageUrl = [String]()
     var conferenceArray = NSArray()
     var conferenceDict = NSDictionary()
     var counter = 0
     var workItem: DispatchWorkItem?
     var iscomingback = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.getConferenceDetails()
        self.tail2.isHidden = false
        self.tial3.isHidden = true
        self.schdule.isHidden = false
        self.nwk.isHidden = false
     
     /*
        let viewwidth = self.view.frame.size.width
        let btnwidth = (viewwidth-60)/4
        self.hotelwidth.constant = btnwidth
        self.hotelheight.constant = btnwidth
        self.travelwidth.constant = btnwidth
        self.travelheight.constant = btnwidth
        self.sightwidth.constant = btnwidth
        self.sightheight.constant = btnwidth
        self.diningwidth.constant = btnwidth
        self.diningheight.constant = btnwidth
        self.othersViewhgt.constant = btnwidth + 15

        var btnImgInset = btnwidth/3
        hotel.imageEdgeInsets = UIEdgeInsets(top: btnImgInset, left: btnImgInset , bottom: btnImgInset, right: btnImgInset)
        hotel.titleEdgeInsets = UIEdgeInsets(top: btnImgInset+20, left: -(btnImgInset+20) , bottom: 0, right: 0)
        */
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let payment = userDefaults.value(forKey: "PaymentDone")
        {
            if "\(payment)" == "true"
            {
                self.backBtn.isEnabled = false
                self.backBtn.tintColor = UIColor.clear
            }
            else
            {
                self.backBtn.isEnabled = true
                self.backBtn.tintColor = UIColor.white
            }
        }
        else{
            self.backBtn.isEnabled = true
            self.backBtn.tintColor = UIColor.white
        }
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.isTranslucent = false
        isfromdetailconf = "true"
    }
    
    func updatevalues()
    {
            if self.conferenceDict.count > 0
            {
           
                self.title = (iskeyexist(dict: conferenceDict, key: "short_name"))
                self.conferencetitle.text = (iskeyexist(dict: conferenceDict, key: ktitle)).htmlToString
                
                self.subtitle.font = UIFont(name: kAppFont, size: 14)
                self.subtitle.textColor = UIColor.black
                let iconsSize = CGRect(x: -2, y: -3, width: 15, height: 15)
                let emojisCollection = [UIImage(named: "bookmark_Detail")]
                let attributedString = NSMutableAttributedString(string: "")
                let loveAttachment = NSTextAttachment()
                self.subtitle.textAlignment = .center
                loveAttachment.image = emojisCollection[0]
                loveAttachment.bounds = iconsSize
                attributedString.append(NSAttributedString(attachment: loveAttachment))
                attributedString.append(NSAttributedString(string:iskeyexist(dict: self.conferenceDict, key: ksubject)))
                self.subtitle.attributedText = attributedString
                let conf_type = iskeyexist(dict: conferenceDict, key: "conf_type")
                if conf_type == "conference" || conf_type == ""
                {
                    self.conftypeImg.image = UIImage(imageLiteralResourceName: "location_Detail")
                     self.cityLbl.text = iskeyexist(dict: conferenceDict, key: kcity) + " " + "," + iskeyexist(dict: conferenceDict, key: kcountry)
                }
                if conf_type == "webinar"
                {
                    self.conftypeImg.image = UIImage(imageLiteralResourceName: "webinar_Detail")
                    self.cityLbl.text = "Webinar"
                }
                
               
                self.dateLbl.text = dateCompartion(startdate: iskeyexist(dict: conferenceDict, key: kstartdate), enddate: iskeyexist(dict: conferenceDict, key: kenddate))
            }
           else
            {
                    print("conference not avaible")
            }
            
    }
    
    // MARK:- CollectionView Delegate & Datasource Methods
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if sliderImageUrl.count > 0
        {
            return sliderImageUrl.count
        }
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kCell, for: indexPath) as! DetailsCollectionViewCell
          let img = self.sliderImageUrl[indexPath.row]
      
          if img != "null" && img != "" && img != "<null>"
          {
            cell.img.downloadImage(imageURL: img, placeholderImage: UIImage.init(named: "homepage"))
           
          }
         else
          {
            cell.img.downloadImage(imageURL: "", placeholderImage: UIImage.init(named: "homepage"))
          }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
     return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.frame.size
        return CGSize(width: size.width, height: size.height)
    }

    func getConferenceDetails()
    {
        self.view.addActivity()
        let obj = NetworkManager(utility: networkUtility!)
        var id = ""
        if let payment = userDefaults.value(forKey: "PaymentDone")
        {
            if "\(payment)" == "true"
            {
                id = userDefaults.value(forKey: "conf_id") as! String
            }
            else
            {
                id = conferenceDict[kid] as? String ?? ""
                userDefaults.set(id, forKey: "conf_id")
            }
        }
        else
        {
             id = conferenceDict[kid] as? String ?? ""
             userDefaults.set(id, forKey: "conf_id")
        }
              let params = [kid:id]
       
        
              obj.postRequestApiWith(api: kConferenceWithID, params: params){ (response) in
              if response != nil
              {
                let dict = response as? NSDictionary ?? [:]
                if dict.count > 0
                {
                    let status = iskeyexist(dict: dict, key: kstatus)
                    if status == "1"
                    {
                        if dict.value(forKey: "conference") != nil
                        {
                          self.conferenceArray = dict.value(forKey: "conference") as? NSArray ?? []
                            if self.conferenceArray.count > 0
                            {
                                self.conferenceDict = self.conferenceArray.firstObject as! NSDictionary
                               // let enddate = self.conferenceDict.value(forKey: "end_date") as! String
                               // self.checkconferenceend(enddate: enddate)
                                self.updatevalues()
                            }
                        }
                    }
                        
                    if self.conferenceArray.count > 0
                    {
                        DispatchQueue.main.async {
                        //  self.view.removeActivity()
                          self.loadSliderUrls()
                        }
                    }
                }
                  
              }
              else
              {
                  DispatchQueue.main.async {
                      self.view.removeActivity()
                  }
              }
                  
          }
    }
    func checkconferenceend(enddate:String)
    {
        
        if let payment = userDefaults.value(forKey: "PaymentDone")
               {
                   if "\(payment)" == "true"
                   {
                    
                     let dateFormatter = DateFormatter()
                     dateFormatter.dateFormat = "yyyy-MM-dd"
                     let finaldate = dateFormatter.date(from: enddate)!
                     let dateformatter1 = DateFormatter()
                     dateformatter1.dateFormat = "yyyy-MM-dd"
                     let currentdatestr = dateformatter1.string(from:Date())
                     let dateformatter2 = DateFormatter()
                     dateformatter2.dateFormat = "yyyy-MM-dd"
                    let currentdate = dateformatter2.date(from:currentdatestr)!
                    
                    if ((finaldate.compare(currentdate)).rawValue  < 0)
                    {
                        userDefaults.set("false", forKey: "PaymentDone")
                        if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                            sd.checkstatus()
                        }
                    }
                     
                   }
               }
        else
        {
            
        }
    }
     func scrollToNext()
    {
       
        if counter < sliderImageUrl.count
        {
            if iscomingback == false
            {
            let indexpath = IndexPath(item: counter, section: 0)
            self.collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
            PageControl.currentPage = counter
            counter = counter + 1
            }
            if iscomingback == true
            {
            let indexpath = IndexPath(item: counter-1, section: 0)
            self.collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
            PageControl.currentPage = counter - 1
            counter = counter - 1
                if counter  == 0
               {
                   self.iscomingback = false
               }
            }
        }
        else
        {
            iscomingback = true
            if counter  == sliderImageUrl.count
            {
                counter = counter - 1
            }
                let indexpath = IndexPath(item: counter-1, section: 0)
                self.collectionView.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
                PageControl.currentPage = counter - 1
                counter = counter - 1
                if counter  == 0
                {
                    self.iscomingback = false
                }
    
        }
        functionOne()
    }
    func loadSliderUrls()
    {
        self.sliderImageUrl.removeAll()
        if conferenceArray.count > 0
        {
            for dict in conferenceArray {
                let innerdict = dict as? NSDictionary ?? [:]
                if innerdict.count > 0
                {
                      let url = iskeyexist(dict: innerdict, key: kslider_url)
                    if self.sliderImageUrl.count <= 3
                    {
                        self.sliderImageUrl.append(url)
                    }
                }
               
            }
        }
        DispatchQueue.main.async {
            self.collectionView.reloadData()
            self.PageControl.numberOfPages = self.sliderImageUrl.count
            self.view.removeActivity()
            self.functionOne()
        }
    }
    func functionOne() {
        workItem = DispatchWorkItem(block: {
            self.scrollToNext()
        })
        let delayTime = DispatchTime.now() + 2.0
        DispatchQueue.main.asyncAfter(deadline: delayTime, execute: workItem!)
    }
    

    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
         self.navigationController?.popViewController(animated: true)
    }
    func gotologin(key:String)
    {
        /*let storyBoard = UIStoryboard(name: kMain, bundle: nil)
                               let vc = storyBoard.instantiateViewController(withIdentifier:"LoginVC") as! LoginViewController
                               vc.confDict = self.conferenceDict
                               self.navigationController?.pushViewController(vc, animated: false)*/
        let storyBoard = UIStoryboard(name: kMain, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier:"signin") as! SigninViewController
        vc.confDict = self.conferenceDict
        vc.naviKey = key
        self.navigationController?.pushViewController(vc, animated: false)
    }
   
    @IBAction func btnActions(_ sender: UIButton)
    {
        switch sender.tag {
        case 50:
            //MARK:- Registration
            print(sender.tag)
            if let loggedIn = userDefaults.value(forKey: kLoggedIn)
            {
                if "\(loggedIn)" == "true"
                {
//                    self.showAlertWith(title: "Failure", message: "Already Registared with Conference")
                    let storyBoard = UIStoryboard(name: kMain, bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "Registration") as! RegistrationViewController
                    vc.confDict = self.conferenceDict
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }
                else
                {
                    self.gotologin(key:"Reg")
                }
            }
            else{
                self.gotologin(key:"Reg")
            }
         
            break
        case 51:
            //MARK:- Check-In
            print(sender.tag)
            self.showAlertWith(title: "Alert", message: "Will available in Next Build")
            break
        case 52:
            //MARK:- Brouchure Download
            print(sender.tag)
            if let loggedIn = userDefaults.value(forKey: kLoggedIn)
            {
                if "\(loggedIn)" == "true"
                {
//                    self.showAlertWith(title: "Failure", message: "Already Registared with Conference")
                    let storyBoard = UIStoryboard(name: kMain, bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "brouchure") as! BroucherDownloadVC
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }
                else
                {
                    self.gotologin(key:"brouchure")
                }
            }
            else{
                    self.gotologin(key:"brouchure")
                }
           
            break
        case 53:
            //MARK:- Session Tracks
            
            let storyBoard = UIStoryboard(name: kMain, bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "session") as! SessionTracksVC
            self.navigationController?.pushViewController(vc, animated: true)
            
            break
        case 54:
            //MARK:- Session Recording
            print(sender.tag)
//            self.showAlertWith(title: "Alert", message: "Will available in Next Build")
  
            if let loggedIn = userDefaults.value(forKey: kLoggedIn)
            {
                if "\(loggedIn)" == "true"
                {
            let storyBoard = UIStoryboard(name: kMain, bundle: nil)
                       let vc = storyBoard.instantiateViewController(withIdentifier: "PresentationVC") as! MyPresentationViewController
                       self.navigationController?.pushViewController(vc, animated: true)

                }
                else
                {
                    self.gotologin(key:"presentation")
                }
            }
            else{
                self.gotologin(key:"presentation")
            }
            break
        case 55:
            //MARK:- Submit Abstract
            
            print(sender.tag)
       
            if let payment = userDefaults.value(forKey: kLoggedIn)
            {
                if "\(payment)" == "true"
                {
                   let storyBoard = UIStoryboard(name: kMain, bundle: nil)
                              let vc = storyBoard.instantiateViewController(withIdentifier: "AbstractVC") as! AbstractViewController
                              self.navigationController?.pushViewController(vc, animated: true)
                }
                else
                {
                    
                    self.gotologin(key:"Abs")
                }
            }
            else{
                self.gotologin(key:"Abs")
                         
              //  self.showAlertWith(title: "Failure", message: "Registration is Mandatory")
                
            }
            
            break
        case 56:
            //MARK:-
            print(sender.tag)
            
            self.showAlertWith(title: "Alert", message: "Will available in Next Build")
            break
            
        case 57:
            //MARK:- Feedback
            print(sender.tag)
            
            if let payment = userDefaults.value(forKey: kLoggedIn)
            {
                if "\(payment)" == "true"
                {
                   let storyBoard = UIStoryboard(name: kMain, bundle: nil)
                              let vc = storyBoard.instantiateViewController(withIdentifier: "feedback") as! feedbackVC
                              self.navigationController?.pushViewController(vc, animated: true)
                }
                else
                {
                    
                    self.gotologin(key:"feedback")
                }
            }
            else{
                self.gotologin(key:"feedback")
                
            }
       
            break
        case 58:
            //MARK:- Hotel
            print(sender.tag)
            self.showAlertWith(title: "Alert", message: "Will available in Next Build")
            break
        case 59:
            //MARK:- Travel
            print(sender.tag)
            self.showAlertWith(title: "Alert", message: "Will available in Next Build")
            break
        case 60:
            //MARK:- Sight Seeing
            print(sender.tag)
            self.showAlertWith(title: "Alert", message: "Will available in Next Build")
            break
        case 61:
            //MARK:- Dining
            print(sender.tag)
            self.showAlertWith(title: "Alert", message: "Will available in Next Build")
        default:
            break
        }
    }
    func showAlertWith(title:String,message:String)
        {
            let alertController = UIAlertController(title: title, message:message , preferredStyle: .alert)
            //to change font of title and message.
            let titleFont = [NSAttributedString.Key.font: UIFont(name: kAppFontBold, size: 14.0)!]
            let messageFont = [NSAttributedString.Key.font: UIFont(name: kAppFont, size: 12.0)!]
            
            let titleAttrString = NSMutableAttributedString(string: title, attributes: titleFont)
            let messageAttrString = NSMutableAttributedString(string: message, attributes: messageFont)
            alertController.setValue(titleAttrString, forKey: "attributedTitle")
            alertController.setValue(messageAttrString, forKey: "attributedMessage")
            
            let alertAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                if message == "Registered Successfully"
                {
                    
                }
            })
    //        let titlealertaction = NSMutableAttributedString(string: "OK", attributes: titleFont)
    //        alertController.setValue(titlealertaction, forKey: "attributedTitle")
            alertController.addAction(alertAction)
            present(alertController, animated: true, completion: nil)
        }
    
}
extension UITextField
{
    func setBottomBorder(color:UIColor)
    {
        self.borderStyle = .none;
        self.translatesAutoresizingMaskIntoConstraints = false
        let bottomBorder = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        bottomBorder.backgroundColor = color
        bottomBorder.translatesAutoresizingMaskIntoConstraints = false
        addSubview(bottomBorder)
        
        //Mark: Setup Anchors
        bottomBorder.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        bottomBorder.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        bottomBorder.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        bottomBorder.heightAnchor.constraint(equalToConstant: 1).isActive = true // Set Border-Strength
    }
    
}
extension UIImageView {

func downloadImage(imageURL: String?, placeholderImage: UIImage? = nil) {
    if let imageurl = imageURL {
        self.af_setImage(withURL: NSURL(string: imageurl)! as URL, placeholderImage: placeholderImage) { (imageResult) in
            if let img = imageResult.result.value {
                self.image = img.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
               // self.image = img.resizeImageWith(newSize: self.frame.size)
                self.contentMode = .scaleAspectFill
            }
        }
    } else {
        self.image = placeholderImage
    }
}
}
