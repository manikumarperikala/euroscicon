//
//  ConferencesNavigationVC.swift
//  Conferences
//
//  Created by Manikumar on 25/10/17.
//  Copyright © 2017 Omics. All rights reserved.
//

import UIKit

class ConferencesNavigationVC: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        var fontSize:CGFloat = 0
        if UI_IDIOM == .pad
        {
            fontSize = 18.0
        }
        else{
            fontSize = 14.0
        }
        
        self.navigationBar.barTintColor = kAppNavColor
        self.navigationBar.isTranslucent = false
        self.navigationBar.tintColor = .white
        self.navigationBar.barStyle = .default
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white,NSAttributedString.Key.font:UIFont(name:kAppFontBold, size: fontSize)!]
    }
}
extension UINavigationController
{
    var rootviewController:UIViewController?
    {
        return viewControllers.first
    }
}
