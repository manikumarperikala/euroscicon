//
//  SearchViewController.swift
//  ConferenceSeries
//
//  Created by manikumar on 13/03/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import UIKit
import Kingfisher

class SearchViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITextFieldDelegate{
    
    @IBOutlet weak var collectionView:UICollectionView!
    @IBOutlet weak var search:CustomTextField1!
    var conferenceArray = NSArray()
    var filteredlist = NSArray()
    var searchActive:Bool = false
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.search.setPaddingWithIcon(image: UIImage(named: "search")!)
        self.getConferences()
        search.delegate = self
        
    }
    //MARK:- CollectionView Delegate Methods
       
       func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if searchActive == true
        {
            if self.filteredlist.count > 0
            {
                return self.filteredlist.count
            }
        }
        else
        {
           if conferenceArray.count > 0 {
                              collectionView.backgroundView = nil
                              return conferenceArray.count
            }
                          
        }
        return 0
        }
          
          func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ContentCollectionViewCell
           cell.layer.cornerRadius = 3.0
           cell.layer.borderWidth = 2.0
           cell.layer.borderColor = UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1.0).cgColor
            var dict = NSArray()
            if searchActive == true
            {
                dict = filteredlist
            }
            else
            {
                dict = conferenceArray
            }
            if dict.count > 0
            {
           let innerdict = dict[indexPath.row] as! NSDictionary
      //     cell.IconImg.kf.indicatorType = .activity
      //     cell.IconImg.layer.cornerRadius = 3.0
           if (((innerdict.value(forKey: kiconurl) != nil)))
           {
               let img = (innerdict.value(forKey: kiconurl) as? String ?? "")
               if  ((img != "null" && img != ""))
               {
                    
              //     let imgurl = URL(string:(innerdict.value(forKey: kiconurl) as? String ?? ""))
              //      cell.IconImg.kf.setImage(with: imgurl!)

               }
               else
               {
              //     cell.IconImg.image = UIImage(imageLiteralResourceName: kimage_not_available)
               }

           }
           cell.titleLbl.text = (((innerdict.value(forKey: ktitle))!) as? String ?? " ")?.htmlToString
           cell.dateLbl.text = dateCompartion(startdate: (innerdict.value(forKey: kstartdate) as! String), enddate: (innerdict.value(forKey: kenddate) as! String))
           cell.countryLbl.text = ((innerdict.value(forKey: kcity)) as? String ?? "") + " ," + ((innerdict.value(forKey: kcountry)) as? String ?? "")
           cell.subjectLbl.text = ((innerdict.value(forKey: ksubject)) as? String ?? "")
                (cell.viewWithTag(300) as! UIImageView).image = UIImage(imageLiteralResourceName: "pin")
                (cell.viewWithTag(400) as! UIImageView).image = UIImage(imageLiteralResourceName: "bookmark")
                
            }
             return cell
           
          }
          func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 10
          }
          
          func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
            return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
          }
          
          func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            var height:CGFloat = 140
                  if UI_IDIOM == .pad
                  {
                      height = collectionView.frame.width / 2 - 10
                  }
            return CGSize(width: collectionView.frame.width / 2 - 10, height: height)
          }
          func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
               var dict = NSArray()
               if searchActive == true
               {
                   dict = filteredlist
               }
               if searchActive == false
               {
                   dict = conferenceArray
               }
               let innerdict = dict[indexPath.row] as! NSDictionary
               let storyBoard = UIStoryboard(name: kMain, bundle: nil)
               let vc = storyBoard.instantiateViewController(withIdentifier:kDetails) as! DetailConferenceVC
               vc.conferenceDict = innerdict
               self.navigationController?.pushViewController(vc, animated: true)
           }
       
    //MARK:- Textfield Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.searchActive = false
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.searchActive = true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.view.addActivity()
        if textField.text == ""
           {
               filteredlist = []
               searchActive = false
           }
           else{
         
               self.searchActive = true
               let searchPredicate = NSPredicate(format: "title CONTAINS[C] %@", textField.text!)
               filteredlist = self.conferenceArray.filter { searchPredicate.evaluate(with: $0) } as NSArray
               
           }
           DispatchQueue.main.async {
            if textField.text == ""
            {
                self.searchActive = false
            }
               self.view.removeActivity()
               self.collectionView.reloadData()
           }
        return true
    }
  /*  func textFieldDidChangeSelection(_ textField: UITextField) {
//        if textField.text!.count > 0
//        {
            self.view.addActivity()
                if textField.text == ""
                {
                    filteredlist = []
                    searchActive = false
                    DispatchQueue.main.async {
                        self.view.removeActivity()
                        self.collectionView.reloadData()
                    }
                }
           
                else{
               /*     filteredlist = conferenceArray.filter{

                        if ($0 as? NSDictionary)?.value(forKey: ktitle) != nil
                        {
                            let title = (($0 as? NSDictionary)?.value(forKey: ktitle) as? String ?? "").htmlToString
                            name = title
                        }
                        return (((name.lowercased()).range(of: (textField.text?.lowercased())!) != nil))
                        } as NSArray
                    */
                    self.searchActive = true
                    let searchPredicate = NSPredicate(format: "title CONTAINS[C] %@", textField.text!)
                    filteredlist = self.conferenceArray.filter { searchPredicate.evaluate(with: $0) } as NSArray
                    DispatchQueue.main.async {
                        self.view.removeActivity()
                        self.collectionView.reloadData()
                    }
                    
                }
                DispatchQueue.main.async {
                    self.view.removeActivity()
                    self.collectionView.reloadData()
                }
            
     
    }*/
        //MARK:- To get Conferences list
        func getConferences()
        {
            self.view.addActivity()
            let obj = NetworkManager(utility: networkUtility!)
            let params = ["p":"1"]
            obj.postRequestApiWith(api: kConferenceMobidataall, params: params){ (response) in
            if response != nil
            {
                if response is NSDictionary
                {
                    let dict = response as! NSDictionary
                    let status = iskeyexist(dict: dict, key: kstatus)
                    if status == "1"
                    {
                        if dict.value(forKey: kconferences) != nil
                        {
                            self.conferenceArray = dict.value(forKey: kconferences) as? NSArray ?? []
                        }
                    }
                    
                    if self.conferenceArray.count > 0
                    {
                        DispatchQueue.main.async {
                            self.view.removeActivity()
                            self.collectionView.reloadData()
                        }
                    }
                    else
                    {
                        print("Data Not availble")
                    }
                }
                
            }
            else
            {
                DispatchQueue.main.async {
                    self.view.removeActivity()
                }
            }
                
        }
        }
}
