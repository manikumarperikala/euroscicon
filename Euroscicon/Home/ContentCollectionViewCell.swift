//
//  ContentCollectionViewCell.swift
//  ConferenceSeries
//
//  Created by manikumar on 07/03/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import UIKit

class ContentCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var IconImg: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var subjectLbl: UILabel!
}
