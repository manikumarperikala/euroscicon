//
//  ViewController.swift
//  ConferenceSeries
//
//  Created by manikumar on 06/03/20.
//  Copyright © 2020 Omics. All rights reserved.
//

import UIKit
import Kingfisher

class HomeViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,applyfilters {
   
    @IBOutlet weak var mainImg: UIImageView!
    @IBOutlet weak var collectionView:UICollectionView!
    @IBOutlet weak var menuButton:UIBarButtonItem!
    @IBOutlet weak var search:CustomTextField1!
    @IBOutlet weak var pinImg:UIImageView!
    @IBOutlet weak var bookmarkImg:UIImageView!
    var conferenceArray = NSMutableArray()
    var filteredConference = NSArray()
    var isfilter = String()
    var pageNumber: Int = 0
    var totalPages:Int  = 0
    var Norecords = -1
    var isLoading = false
    var filters = [String:Any]()
    var loadingView: LoadingReusableView?
    var isLastpage = "false"
 

    override func viewDidLoad() {
        super.viewDidLoad()
      //  checkforupdate()
        if revealViewController() != nil
        {
             menuButton.target = revealViewController()
             menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
         view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        self.pageNumber = 1
        self.totalPages = 1
        
        self.isfilter = "false"
        self.search.setPaddingWithIcon(image: UIImage(named: "filter_Home")!)
        self.search.isHidden = false
        self.search.addTarget(self, action: #selector(myTargetFunction), for: UIControl.Event.touchDown)
        let loadingReusableNib = UINib(nibName: "LoadingReusableView", bundle: nil)
        collectionView.register(loadingReusableNib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "loadingresuableviewid")
        
    }
    func checkforupdate()
    {
        DispatchQueue.global().async {
            do {
                let update = try isUpdateAvailable()
                
                print("update",update)
                DispatchQueue.main.async {
                    if update{
                        self.popupUpdateDialogue();
                    }
                    
                }
            } catch {
                print(error)
            }
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
       return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.pageNumber = 1
        self.isLastpage = "false"
       
        if isfiltersapplied == "false"
        {
            if isfromdetailconf == "false"
            {
                self.conferenceArray.removeAllObjects()
                self.getConferences(params: ["filters":isfiltersapplied,"page":pageNumber])
            }
            isfromdetailconf = "false"
         }
        else{
            self.conferenceArray.removeAllObjects()
            if self.filters.count > 0
            {
                self.getConferences(params: self.filters)
            }
            else{
                isfiltersapplied = "false"
                 self.getConferences(params: ["filters":isfiltersapplied,"page":pageNumber])
            }
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.barStyle = .black
    }
    
    //MARK:- CollectionView Delegate Methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if conferenceArray.count > 0 {
                          collectionView.backgroundView = nil
                          return conferenceArray.count
        }
        else
        {
            let messageLabel = UILabel(frame: collectionView.frame)
            messageLabel.text = "No data available."
            messageLabel.textAlignment = .center
            collectionView.backgroundView = messageLabel
        }
                      return 0
       }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ContentCollectionViewCell
        cell.layer.cornerRadius = 3.0
        cell.layer.borderWidth = 2.0
        cell.layer.borderColor = UIColor(red: 154/255, green: 141/255, blue: 68/255, alpha: 1.0).cgColor
        let innerdict = conferenceArray[indexPath.row] as! NSDictionary
//        cell.IconImg.kf.indicatorType = .activity
//        cell.IconImg.layer.cornerRadius = 3.0
        if (((innerdict.value(forKey: kiconurl) != nil)))
        {
            let img = (innerdict.value(forKey: kiconurl) as? String ?? "")
            if  ((img != "null" && img != "" && img != "<null>"))
            {
                 
//                let imgurl = URL(string:(innerdict.value(forKey: kiconurl) as? String ?? ""))
//                 cell.IconImg.kf.setImage(with: imgurl!)

            }
            else
            {
//                cell.IconImg.image = UIImage(imageLiteralResourceName: kimage_not_available)
            }

        }
        cell.titleLbl.text = (((innerdict.value(forKey: ktitle))!) as? String ?? " ")?.htmlToString
//        cell.titleLbl.textColor = UIColor(red: 0, green: 64/255, blue: 128/255, alpha: 1.0)
        cell.dateLbl.text = dateCompartion(startdate: (innerdict.value(forKey: kstartdate) as! String), enddate: (innerdict.value(forKey: kenddate) as! String))
        
        cell.subjectLbl.text = ((innerdict.value(forKey: ksubject)) as? String ?? "")
        
        let conf_type = iskeyexist(dict: innerdict, key: "conf_type")
        if conf_type == "conference"
        {
            (cell.viewWithTag(300) as! UIImageView).image = UIImage(imageLiteralResourceName: "location_Home")
            cell.countryLbl.text = ((innerdict.value(forKey: kcity)) as? String ?? "") + "," + ((innerdict.value(forKey: kcountry)) as? String ?? "")
        }
        if conf_type == "webinar"
        {
          (cell.viewWithTag(300) as! UIImageView).image = UIImage(imageLiteralResourceName: "webinar")
          cell.countryLbl.text = "Webinar"
        }
        (cell.viewWithTag(400) as! UIImageView).image = UIImage(imageLiteralResourceName: "bookmark_Home")
          return cell
        
       }
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
           return 10
       }
       
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
       }
       
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            
        var height:CGFloat = 140
        if UI_IDIOM == .pad
        {
            height = collectionView.frame.width / 4 - 10
            return CGSize(width: collectionView.frame.width / 4 - 10, height: height)
       
        }else{
            return CGSize(width: collectionView.frame.width / 2 - 10, height: height)

        }
       }
       func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            let innerdict = conferenceArray[indexPath.row] as? NSDictionary ?? [:]
            let storyBoard = UIStoryboard(name: kMain, bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier:kDetails) as! DetailConferenceVC
            vc.conferenceDict = innerdict
          
            
            self.navigationController?.pushViewController(vc, animated: false)
        }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if self.conferenceArray.count > 0 && isLastpage == "false"
        {
            return CGSize(width: collectionView.bounds.size.width, height: 55)
        }
        else
        {  return CGSize.zero
            
        }
      /*  if self.isLoading {
                   return CGSize.zero
               } else {
                   return CGSize(width: collectionView.bounds.size.width, height: 55)
               }*/
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionFooter {
            let aFooterView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "loadingresuableviewid", for: indexPath) as! LoadingReusableView
            loadingView = aFooterView
            loadingView?.backgroundColor = UIColor.clear
           
             return aFooterView
        
        }
        return UICollectionReusableView()
    }
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
            if elementKind == UICollectionView.elementKindSectionFooter {
                
                  self.loadingView?.activityIndicator.startAnimating()
            
            }
        }
    
        func collectionView(_ collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, at indexPath: IndexPath) {
            if elementKind == UICollectionView.elementKindSectionFooter {
                self.loadingView?.activityIndicator.stopAnimating()
                self.loadingView?.removeFromSuperview()
            }
        }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
            if indexPath.row == conferenceArray.count - 1 && !self.isLoading && isLastpage == "false" {
               loadMoreData()
                
            }
        }
    
        func loadMoreData() {
            if !self.isLoading {
                
                self.isLoading = true
                self.insertRowAtBottom()
                DispatchQueue.global().async {
                    // Fake background loading task for 2 seconds
                    sleep(3)
                    // Download more data here
//                    DispatchQueue.main.async {
//                        self.collectionView.reloadData()
//                        self.isLoading = false
//                    }
                }
            }
        }
    
       
    
    //MARK:- To get Conferences list
    func getConferences(params:[String:Any])
    {
        if self.pageNumber == 1
        {
             self.view.addActivity()
        }
       
        let obj = NetworkManager(utility: networkUtility!)
        let params = params
        obj.postRequestApiWith(api: kConferenceMobidataall, params: params){ (response) in
        if response != nil
        {
            if response is NSDictionary
            {
                let dict = response as! NSDictionary
                let status = iskeyexist(dict: dict, key: kstatus)
                if status == "1"
                {
                    let totalpage = iskeyexist(dict: dict, key: "total_pages")
                    self.totalPages = Int(totalpage) ?? 1
                    if self.totalPages == self.pageNumber
                    {
                        self.isLastpage = "true"
                    }
                   
                    if (dict.value(forKey: kconferences) != nil)
                    {
                        if dict.value(forKey: kconferences) is NSArray
                        {
                            let confarr = dict.value(forKey: kconferences) as? NSArray ?? []
                            for dict in confarr
                            {
                                self.conferenceArray.add(dict)
                            }
                            
                        }
                    }
                    
                }
                else
                {
                    print("status failed")
                }
                if self.conferenceArray.count > 0
                {
                    DispatchQueue.main.async {
                        self.view.removeActivity()
                        self.collectionView.reloadData()
                        self.isLoading = false
                    }
                }
                else
                {
                    self.pageNumber = self.Norecords
                }
            }
            
        }
        else
        {
           
            DispatchQueue.main.async {
                self.view.removeActivity()
            }
        }
            
       }
    }
    

    @IBAction func filterAction(_ sender: UIBarButtonItem) {
        let storyBoard = UIStoryboard(name: kMain, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier:"filter") as! FiltersViewController
        vc.filterdelegate = self
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
      
        func insertRowAtBottom() {
            if pageNumber != Norecords {
                pageNumber += 1
                if isfiltersapplied == "false"
                {
                    if pageNumber <= totalPages
                    {
                        isLastpage = "false"
                        getConferences(params: ["filters":isfiltersapplied,"page":pageNumber])
                        
                    }
                    else
                    {
                        isLoading = false
                        isLastpage = "true"
                    }
                }
                else
                {
                    if pageNumber <= totalPages
                   {
                    self.filters.updateValue(pageNumber, forKey: "page")
                    self.getConferences(params: self.filters)
                   }
                    else
                    {
                        isLoading = false
                        isLastpage = "true"
                    }
                
                }
                
               
            }
        }
    @objc func myTargetFunction()
    {
        
        let storyBoard = UIStoryboard(name: kMain, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier:"filter") as! FiltersViewController
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func filterData(conferencearr: [String:Any], isfilter: String) {
        self.filters = conferencearr
//        self.getConferences(params: self.filters)
      
     }
    func popupUpdateDialogue(){
           var versionInfo = kAppStoreVersion
           
           let alertMessage = "A new version of Conference Series Application is available,Please update to version "+versionInfo;
        let alert = UIAlertController(title: "New Version Available", message: alertMessage, preferredStyle: UIAlertController.Style.alert)
     
           let okBtn = UIAlertAction(title: "Update", style: .default, handler: {(_ action: UIAlertAction) -> Void in
               if let url = URL(string: kAppstoreLink),
                   UIApplication.shared.canOpenURL(url){
                   if #available(iOS 10.0, *) {
                       UIApplication.shared.open(url, options: [:], completionHandler: nil)
                   } else {
                       UIApplication.shared.openURL(url)
                   }
               }
           })
           let noBtn = UIAlertAction(title:"Skip this Version" , style: .destructive, handler: {(_ action: UIAlertAction) -> Void in
           })
           alert.addAction(okBtn)
           alert.addAction(noBtn)
           self.present(alert, animated: true, completion: nil)
           
       }
} 
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
class CustomTextField1:UITextField
{
    var imageIconView = UIImageView()
    var paddingview = UIView()
    let padding = 8
    let size = 20
    override func awakeFromNib()
    {
        super.awakeFromNib()
        paddingview = UIView(frame: CGRect(x: 0, y: 0, width: size+padding, height: size))
        imageIconView = UIImageView(frame: CGRect(x: 10, y:paddingview.frame.size.height/2-(10), width: 20, height: 20))
        paddingview.addSubview(imageIconView)
        self.leftViewMode = .always
        self.leftView = paddingview
    }
    
    func setPaddingWithIcon(image:UIImage)
    {
        imageIconView.image = image
    }
    
    deinit {
        
    }
}
